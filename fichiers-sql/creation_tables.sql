-- INFOS COMMANDES MYSQL
-- voir toutes les databases : show databases
-- pour utiliser la base : \u nomdedatabase
-- pour creer la database : create database nomdatabase
-- pour le reste : IDEM QUE ORACLE
-- sequence <=> autoincrement

-- SUPPRESSION DES TABLES
drop table if exists utilisateur;
drop table if exists adherent;
drop table if exists gestionnaire;

drop table if exists livre;
drop table if exists musique;
drop table if exists film;
drop table if exists oeuvre;
drop table if exists exemplaire;


-- UTILISATEURS

create table adherent(
	idA int primary key auto_increment,
	loginA varchar(255) not null unique,
	nom varchar(255) not null,
	prenom varchar(255) not null,
	telephone int not null,
	email varchar(255) not null unique,
	mdp varchar(255) not null
);

create table gestionnaire(
	idG int primary key auto_increment,
	loginG varchar(255) not null unique,
	nom varchar(255) not null,
	prenom varchar(255) not null,
	mdp varchar(255) not null
);


-- MEDIAS

create table oeuvre(
	idO int primary key auto_increment,
	titre varchar(255) not null,
	auteur varchar(255) not null,
	prix int not null,
	nom_img varchar(255) not null,
	check (prix > 0 and prix <= 200)
);


create table livre(
	idL int primary key, 
	typeL varchar(20) not null,
	foreign key (idL) references oeuvre(idO),
	check (typeL = 'BD' or typeL = 'Journaux' or typeL = 'Magazines' or typeL = 'Romans')
);


create table musique(
	idM int primary key, 
	typeM varchar(20) not null,
	foreign key (idM) references oeuvre(idO),
	check (typeM = 'Electro' or typeM ='Pop' or typeM ='Rap' or typeM='Rnb' or typeM ='Rock')
);


create table film(
	idF int primary key, 
	typeF varchar(20) not null,
	foreign key (idF) references oeuvre(idO),
	check (typeF ='Action' or typeF ='Comedie' or typeF ='Horreur' or typeF ='Horreur' or typeF ='Policier' or typeF ='Romance')
);


create table exemplaire(
	idE int primary key auto_increment,
	idO int not null,
	etat varchar(20) default 'disponible',
	foreign key (idO) references oeuvre(idO),
	check (etat= 'disponible' or etat='reserve' or etat='emprunte' or etat='achete')
);


-- ACTIONS

create table emprunt(
	idA int not null,
	idE int not null unique,
	dateEmp datetime not null,
	dateRet datetime not null,
	prolongation varchar(10) default 'false',
	foreign key (idA) references adherent(idA),
	foreign key (idE) references exemplaire(idE),
	check (prolongation ='true' or prolongation ='false')
);

create table reservation(
	idA int not null,
	idE int not null unique,
	dateRes datetime not null,
	foreign key (idA) references adherent (idA),
	foreign key (idE) references exemplaire(idE)
);

-- insert into reservation values (8, 1, now());

create table achat(
	idA int not null,
	idE int not null unique,
	dateAch datetime not null,
	foreign key (idA) references adherent (idA),
	foreign key (idE) references exemplaire(idE)
);