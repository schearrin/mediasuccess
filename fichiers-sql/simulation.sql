

delete from emprunt;

-- emprunt avec retard
insert into emprunt(idA,idE,dateEmp,dateRet) values(2,70, now()- interval 23 day, now()- interval 2 day);
update exemplaire set etat="emprunte" where idE = 70;

-- emprunt avec retard de plus de 7 jours
insert into emprunt(idA,idE,dateEmp,dateRet) values(3,69, now()- interval 40 day, now()- interval 19 day);
update exemplaire set etat="emprunte" where idE = 69;

