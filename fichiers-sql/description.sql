create table description(
	idO int, 
	synopsis text,
	foreign key (idO) references oeuvre(idO)
);


insert into description values (1,
	'Petit voleur des rues du califat d\'Abu,
	 Mehdi est contraint de se présenter bien malgré lui à l\'examen d\'entrée d\'Hikmadrassa,
	  la plus célèbre école d\'alchimie du continent. 
	  Même s\'il n\'y connaît rien en enchantements, sortilèges et autres invocations de golams,
	   il va néanmoins se découvrir un pouvoir quasi unique...');