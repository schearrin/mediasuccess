<?php
  	// genere un login selon si l'utilisateur est adherent ou gestionnaire en verifiant qu'elles sont uniques
function generelogin($utilisateur, $p, $n, $t)
{
    
  $p = preg_replace('/\s+/', '0', $p);
  $p = preg_replace('/-+/', '0', $p);

  $premier_p = substr($p, 0, 2);
  $premier_n = substr($n, 0, 1);
  $dernier_t = substr($t, -5);

  $login = $premier_p.$premier_n.$dernier_t;

  if($utilisateur== 'adherent'){
   while(verif_uniqueloginadherent($login)){
     $premier_p = substr($p, 0, 2);
     $premier_n = substr($n, 0, 1);
     $dernier_t = substr($t, -5);

     $login = $premier_p.$premier_n.$dernier_t;
   }

   return $login;
 }
 if($utilisateur== 'gestionnaire')
 {
  while (verif_uniquelogingestionnaire($login)) {
    $premier_p = substr($p, 0, 2);
    $premier_n = substr($n, 0, 1);
    $dernier_t = substr($t, -5);

    $login = $premier_p.$premier_n.$dernier_t;
  }
  return $login;

}
return 'Erreur, type utilisateur inconnu';
};


    //fonction envoie d'email lors de l'inscription de l'adherent
function email_mediathequetoadherent($loginadh, $nom, $prenom, $email){

  $headers  = "From: \"Mediatheque\"<contact@projetGLAL3MIAGE-mediatheque.com>\n";
  $headers .= "Reply-To: contact@projetGLAL3MIAGE-mediatheque.com\n";
  $headers .= "X-Priority: 1\n";
  $headers .= "Content-Type: text/plain; charset=\"iso-8859-1\"\n";
  $headers .= "Content-Transfer-Encoding: 8bit";

  $subject  = "Bienvenue sur Mediatheque ".$prenom." ".$nom." !" ;

  $message  = "<html><head><title>Bienvenue ".$prenom." ".$nom.", </title></head><body>Voici votre identifiant :<br />< br />".$login."<br /><br />Votre Mediatheque.</body></html>";

  $result = mail($email, $subject, $message, $headers);

}


   //fonction envoie d'email de visiteur vers mediatheque
function email_visiteurtomediatheque($nom, $email, $telephone, $message){

  $headers  = "From: \"Mediatheque\"<".$email.">\n";
  $headers .= "Reply-To: ".$email."\n";
  $headers .= "X-Priority: 1\n";
  $headers .= "Content-Type: text/plain; charset=\"iso-8859-1\"\n";
  $headers .= "Content-Transfer-Encoding: 8bit";

  $subject  = "Message du visiteur : ".$nom ;

  $message  = "<html><head><title>Message du visiteur ".$nom." : </title></head><body><br />< br />".$message."<br /><br />"."Contact : <br />".$email."<br />".$telephone."</body></html>";

  $tomediatheque = "contact@projetGLAL3MIAGE-mediatheque.com";
  $result = mail($mediatheque, $subject, $message, $headers);

}

  // affiche la collection d'oeuvres
function aff_getalloeuvres($typeoeuvre, $typetypeoeuvre){
                    $tab = get_alloeuvres($typeoeuvre,$typetypeoeuvre);
                  foreach ($tab as $ligne) {

                     echo '<div class="col-sm-4">';
                     echo '<div class="product">';
                     echo '<a href="#">';
                     echo '<img src="images/products/'.$typeoeuvre.'/'.$typetypeoeuvre.'/'.$ligne['nom_img'].'.jpg" class="img-responsive"  alt="product">';
                     echo '<span class="price special">'.$ligne['prix'].' €'.'</span>';                                 
                     echo '</a>';

                     echo '<div class="overlay">';
                     echo '<div class="detail">';
                     echo '<h4><a href="#">'.$ligne['titre'].'</a></h4>';
                     echo '<a href="product.php?idoeuvre='.$ligne['idO'].'&typeoeuvre='.$typeoeuvre.'&typetypeoeuvre='.$typetypeoeuvre.'" class="btn btn-default view animated fadeInLeft"><i class="fa fa-search-plus"></i> Information</a>';                              
                     echo '</div>';
                     echo '</div>';
                     echo '</div>';
                     echo '</div>';
              }
}


// affiche les exemplaires disponibles
function aff_exemplaire($id){
  $tab = get_exemplaire($id);
             echo '<table class="compte-menu-vertical" style="font-size:125%;">';
         echo '<tr>';
          echo '<td><b>ID Oeuvre</b></td>';
          echo '<td><b>ID Exemplaire</b></td>';
          echo '<td><b>Etat</b></td>';
        echo '</tr>';
  foreach ($tab as $ligne) {
        echo '<tr>';
          echo '<td>'.$ligne['idO'].'</td>';
          echo '<td>'.$ligne['idE'].'</td>';
          echo '<td>'.$ligne['etat'].'</td>';
        echo '</tr>';
          }

      echo '</table>';
}

?>