<?php include 'header.php';?> 

<div class="container">

  <div class="content inside-page create-account">
    <h2 class="title">Déconnexion</h2>
    <div class="row">


      <?php
  //deconnexion de l'adherent ou du gestionnaire en supprimant les variables de la session courante
      if(isset($_SESSION['logadherent']) || isset($_SESSION['loggestionnaire']))
      {
            //unset($_SESSION['loggestionnaire'], $_SESSION['userid']);
        session_unset();
        ?>
        <div class="form-horizontal col-sm-6 col-sm-offset-3">
          <div class="panel-body">
            <h4>Déconnexion réussie</h4>
            <br /><br />
            <div class="message">Vous avez bien &eacute;t&eacute; d&eacute;connect&eacute;. Vous allez &ecirc;tre rediriger vers la page d'accueil.<br /></div>
            <br /><br />
          </div>
        </div>
        <META HTTP-EQUIV="Refresh" CONTENT="2;URL= index.php" />
        <?php
      }

      ?>

    </div>
  </div>
</div>

<?php include 'footer.php';?>