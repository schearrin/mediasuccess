<?php include 'header.php'; ?>

<div class="content fullpage homepage">

    <!-- banner & block -->
    <div class="col-md-6 col-sm-8  banner height-full  animated fadeInDownBig">
    	<div id="BannerCarousel" class="carousel slide  height-full" data-ride="carousel">
          <div class="carousel-inner  height-full">

            <!-- 1 -->
             <div class="item active  height-full">
                <img src="images/slide1.jpg" class="img-responsive" alt="slide">
                <div class="caption">
                    <?php
                    if(isset($_SESSION['logadherent']))
                    {
                        ?>
                        <!-- acces adherent -->
                        <h2 class="animated fadeInDown">Bonjour</h2>
                        <p class="animated fadeInUp"><?php echo strtoupper($_SESSION['adherentprenom']); ?> <?php echo strtoupper($_SESSION['adherentnom']); ?></p>
                        <?php
                    }
                    elseif(isset($_SESSION['loggestionnaire']))
                    {
                        ?>
                        <!-- acces gestionnaire -->
                         <h2 class="animated fadeInDown">Bonjour</h2>
                        <p class="animated fadeInUp"><?php echo strtoupper($_SESSION['gestionnaireprenom']); ?> <?php echo strtoupper($_SESSION['gestionnairenom']); ?></p>
                        <?php
                    }
                    else{
                        ?>
                        <!-- acces visiteur -->
                        <h2 class="animated fadeInDown">Bienvenue sur Mediatheque</h2>
                           <p class="animated fadeInUp">Vous y trouverez livres, musiques et films pour tous âges.</p>
                        <?php
                    }
                    ?>
                    <a href="horaire.php" class="btn btn-default animated fadeInLeftBig">Horaires</a> <a href="contact.php" class="btn btn-default animated fadeInRightBig">Adresse & Contact</a>
                </div>
            </div>
            <!-- fin 1 -->

            <!-- 2 -->
            <div class="item  height-full">
                <img src="images/slide2.jpg"  class="img-responsive" alt="slide">
                <div class="caption">                    
                    <?php
                    if(isset($_SESSION['logadherent']))
                    {
                        ?>
                        <!-- acces adherent -->
                        <h2 class="animated fadeInDown">Bonjour</h2>
                        <p class="animated fadeInUp"><?php echo strtoupper($_SESSION['adherentprenom']); ?> <?php echo strtoupper($_SESSION['adherentnom']); ?></p>
                        <?php
                    }
                    elseif(isset($_SESSION['loggestionnaire']))
                    {
                        ?>
                        <!-- acces gestionnaire -->
                         <h2 class="animated fadeInDown">Bonjour</h2>
                        <p class="animated fadeInUp"><?php echo strtoupper($_SESSION['gestionnaireprenom']); ?> <?php echo strtoupper($_SESSION['gestionnairenom']); ?></p>
                        <?php
                    }
                    else{
                        ?>
                        <!-- acces visiteur -->
                        <h2 class="animated fadeInDown">Bienvenue sur Mediatheque</h2>
                           <p class="animated fadeInUp">Vous y trouverez livres, musiques et films pour tous âges.</p>
                        <?php
                    }
                    ?>
                    <a href="horaire.php" class="btn btn-default animated fadeInLeftBig">Horaires</a> <a href="contact.php" class="btn btn-default animated fadeInRightBig">Adresse & Contact</a>
                </div>
            </div>
            <!-- fin 2 -->

        </div>
        <!-- Controls -->
        <!--<a class="left carousel-control" href="#BannerCarousel" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
        <a class="right carousel-control" href="#BannerCarousel" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>-->
    </div>
</div>



<!-- -->
<!--
<div class="col-md-3 col-sm-4 height-full"> 
    <div class="height-full info animated fadeInDown">
       <div class="col-md-12 col-xs-12 ">
        <div class="block height-full">
            <div class="store-info" style="height:50%">
                <div class="desc">
                    <br /><br />
                    <a href="horaire.php"  class="btn btn-default "><h4>Horaires</h4><br /></a>
                    <p style="color:white"><br />La mediatheque ouvre ses portes du lundi au vendredi de 10h a 20h.</p>
                </div>
            </div>

            <div class="store-info" style="height:50%">
                <div class="desc">
                    <br /><br />
                    <a href="contact.php"  class="btn btn-default "><h4>Contact<br />&<br />Adresse</h4><br /></a>
                    <p style="color:white">La mediatheque ouvre ses portes du lundi au vendredi de 10h a 20h.</p>
                </div>
            </div>
        </div>

    </div> -->
<!--<div class="col-md-6 col-xs-12 height-half animated fadeInUp"><div class="block height-full women "><a href="#" class="info animated fadeInDown"><h3>Horaires</h3><span></span></a></div></div> -->
<!--<div class="col-md-6 col-xs-12 height-half animated fadeInDown"><div class="block height-full store"><div class="overlay"><div class="store-info"><div class="desc"><h4>Over 2500 products</h4><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p><a href="contact.php"  class="btn btn-default">our store</a></div></div></div></div></div>-->
<!--<div class="col-md-6 col-xs-12 height-half animated fadeInDown"><div class="block height-full men"><a href="#" class="info animated fadeInDown"><h3>Musiques</h3><span></span></a></div></div> -->
<!--<div class="col-md-6 col-xs-12 height-half animated fadeInUp"><div class="block height-full couple"><a href="#" class="info animated fadeInDown"><h3>Contact</h3><span></span></a></div></div>-->
<!--</div>
</div> -->
<!-- -->

<!-- banner & block -->
</div>


<?php include 'footer.php';?>