<?php include 'header.php';?>

<div class="container">
  <div class="content inside-page about">
   <div class="breadcrumb"><a href="index.php">Home</a> / Administration</div>

   <!-- titre -->
   <h2 class="title">Administration</h2>

   
   <div class="row">
     <!-- ce qui se trouve sur la gauche -->
     <div class="col-sm-4">
      <h3>Profil</h3>
      <ul class="compte-menu-vertical">
        <li class="compte-mv-item"><a href="cptegest_informations.php">Informations</a></li>
      </ul>
      <br /><br />

      <h3>Menu</h3>
      <ul class="compte-menu-vertical">
        <li class="compte-mv-item"><a href="cptegest_createaccountadh.php">Créer Adhérent</a></li>
        <li class="compte-mv-item"><a href="cptegest_achat.php">Enregistrer un achat</a></li>
        <li class="compte-mv-item"><a href="cptegest_emprunt.php">Enregistrer un emprunt</a></li>
        <li class="compte-mv-item"><a href="cptegest_retour.php">Enregistrer un retour</a></li>
      </ul>
      <br /><br />
    </div>

    <!-- ce qui se trouve sur la droite -->  	
    <div class="col-sm-8">
      <h3>Informations</h3>
      <p>
        <h4 align="center">Bonjour <?php echo $_SESSION['gestionnaireprenom']; ?> <?php echo $_SESSION['gestionnairenom']; ?> !</h4>
      </p>

        <div class="location col-sm-10 col-sm-offset-1 col-sm-12">

          <table class="compte-menu-vertical" style="font-size:125%;">
           <tr>
            <td><b>Login</b></td>
            <td><?php echo $_SESSION['loggestionnaire']; ?></td>
          </tr>
          <tr>
            <td><b>Nom</b></td>
            <td><?php echo $_SESSION['gestionnairenom']; ?></td>
          </tr>
          <tr>
            <td><b>Prénom</b></td>
            <td><?php echo $_SESSION['gestionnaireprenom']; ?></td>
          </tr>
        </table>

       </div>

    </div>

  </div>


</div>
</div>
<?php include 'footer.php';?>