<?php include 'header.php';?>
<div class="container">
  <div class="content inside-page about">
    <div class="breadcrumb"><a href="index.php">Home</a> / Mon compte</div>

    <!-- titre -->
    <h2 class="title">Mon compte</h2>

    <div class="row">
      <!-- ce qui se trouve sur la gauche -->
    <div class="col-sm-4">
      <h3>Profil</h3>
      <ul class="compte-menu-vertical">
        <li class="compte-mv-item"><a href="cpteadh_informations.php">Informations</a></li>
      </ul>
      <br /><br />

      <h3>Menu</h3>
      <ul class="compte-menu-vertical">
        <li class="compte-mv-item"><a href="cpteadh_reservation.php">Liste des réservations</a></li>
        <li class="compte-mv-item"><a href="cpteadh_prolongation.php">Liste des emprunts</a></li>
        <li class="compte-mv-item"><a href="cpteadh_achat.php">Historique d'achat</a></li>
      </ul>
      <br /><br />
    </div>
      <!-- ce qui se trouve sur la droite -->  	
      <div class="col-sm-8">


        <h3>Liste des réservations</h3>
        <br />
        <?php $tab = listereservation($_SESSION['userid']); ?>
        <div class="location col-sm-13 col-sm-offset-1">
            <table class="compte-menu-vertical" style="font-size:125%;">
            <tr>
              <td><b>Id</b></td>
              <td><b>Titre</b></td>
              <td><b>Auteur</b></td>
              <td><b>Genre</b></td>
              <td><b>Date réservation</b></td>
              <td><b>Annuler</b></td>
            </tr>
            <?php
            foreach ($tab as $ligne) {
              if( ($ligne['typeL']=='BD') || ($ligne['typeL']=='Journaux') || ($ligne['typeL']=='Magazines') || ($ligne['typeL']=='Romans')){
                echo '<tr>';
                echo '<td>'.$ligne['idE'].'</td>';
                echo '<td>'.$ligne['titre'].'</td>';
                echo '<td>'.$ligne['auteur'].'</td>';
                echo '<td>'.$ligne['typeL'].'</td>';
                echo '<td>'.$ligne['dateRes'].'</td>';
                echo '<td><a href="product.php?idoeuvre='.$ligne['idO'].'&typeoeuvre=livre&typetypeoeuvre='.$ligne['typeL'].'">[ X ]</a></td>';
                echo '</tr>';
              }
              if( ($ligne['typeL']=='Electro') || ($ligne['typeL']=='Pop') || ($ligne['typeL']=='Rap') || ($ligne['typeL']=='Rnb') || ($ligne['typeL']=='Rock')){
                echo '<tr>';
                echo '<td>'.$ligne['idE'].'</td>';
                echo '<td>'.$ligne['titre'].'</td>';
                echo '<td>'.$ligne['auteur'].'</td>';
                echo '<td>'.$ligne['typeL'].'</td>';
                echo '<td>'.$ligne['dateRes'].'</td>';
                echo '<td><a href="product.php?idoeuvre='.$ligne['idO'].'&typeoeuvre=musique&typetypeoeuvre='.$ligne['typeL'].'">[ X ]</a></td>';
                echo '</tr>';
              }
              if( ($ligne['typeL']=='Action') || ($ligne['typeL']=='Comedie') || ($ligne['typeL']=='Horreur') || ($ligne['typeL']=='Policier') || ($ligne['typeL']=='Romance')){
                echo '<tr>';
                echo '<td>'.$ligne['idE'].'</td>';
                echo '<td>'.$ligne['titre'].'</td>';
                echo '<td>'.$ligne['auteur'].'</td>';
                echo '<td>'.$ligne['typeL'].'</td>';
                echo '<td>'.$ligne['dateRes'].'</td>';
                echo '<td><a href="product.php?idoeuvre='.$ligne['idO'].'&typeoeuvre=film&typetypeoeuvre='.$ligne['typeL'].'">[ X ]</a></td>';
                echo '</tr>';
              }
            }
           ?>
          </table>
          <br />
          <p align="center"><b>/!\ Toutes les réservations sont à retirer auprès du gestionnaire avant les 48h qui suivent la date de réservation.</b></p>
            <br />
        </div>


    </div>
  </div>


</div>
</div>
</div>
<?php include 'footer.php';?>