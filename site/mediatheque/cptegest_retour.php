<?php include 'header.php';?> 


<div class="container">
	<!-- form -->
	<div class="content inside-page create-account about contact">
		<div class="breadcrumb"><a href="index.php">Home</a> / Administration</div>
		<h2 class="title">Administration</h2>

		
		<div class="row">
			<!-- Menu de gauche -->
			<div class="col-sm-4">
				<h3>Profil</h3>
				<ul class="compte-menu-vertical">
					<li class="compte-mv-item"><a href="cptegest_informations.php">Informations</a></li>
				</ul>
				<br /><br />

				<h3>Menu</h3>
      <ul class="compte-menu-vertical">
        <li class="compte-mv-item"><a href="cptegest_createaccountadh.php">Créer Adhérent</a></li>
        <li class="compte-mv-item"><a href="cptegest_achat.php">Enregistrer un achat</a></li>
        <li class="compte-mv-item"><a href="cptegest_emprunt.php">Enregistrer un emprunt</a></li>
        <li class="compte-mv-item"><a href="cptegest_retour.php">Enregistrer un retour</a></li>
      </ul>
				<br /><br />
			</div>


			<!-- Affichage droite -->
			<div class="col-sm-8">
				<h3>Enregistrer un retour</h3><br /><br />

				<?php

				if(isset($_SESSION['loggestionnaire'])){

					if(isset($_POST['idoeuvre'], $_POST['login'])){

					 $_POST['idoeuvre'] = mysqli_real_escape_string($connexionbdd, $_POST['idoeuvre']);
					 $_POST['login'] = mysqli_real_escape_string($connexionbdd, $_POST['login']);

						if(verif_adherent($_POST['login'])){

							$idoeuvre = $_POST['idoeuvre'];
							$login = $_POST['login'];					
							if(retour($idoeuvre,$login)){

								if(diff_datenowretour($idoeuvre) <=0 ){
									echo '<p>Le retour a bien été enregistré. Retard : aucun </p>';
								}
								if(diff_datenowretour($idoeuvre) >= 1 && diff_datenowretour($idoeuvre) <= 7 ){
									echo '<p>Le retour a bien été enregistré. Retard : oui, Paiement : 5 €. </p>';
								}
								if(diff_datenowretour($idoeuvre) > 7){
									$prix = get_prix($idoeuvre);
									echo '<p>Le retour a bien été enregistré. Retard : perte, Paiement : '.$prix.'. </p>';
								}
							}else{
								echo '<p>Vous vous êtes trompé d\'exemplaire.</p>';

							}
						}else{
							echo "<p>L'adhérent n'existe pas.</p>";
						}

					}
				}
				?>

	<!-- affiche le formulaire -->
	<form class="form-horizontal col-sm-10 col-sm-offset-1" method="post" action="cptegest_retour.php">
		<div class="panel-body">

			<div class="form-group">
				<label for="inputEmail3" class="col-sm-4 control-label">Exemplaire</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="inputEmail3" name="idoeuvre" value="<?php if(isset($_POST['idoeuvre'])){echo htmlentities($_POST['idoeuvre'], ENT_QUOTES, 'UTF-8');} ?>" />
				</div>				
			</div>

			<div class="form-group">
				<label for="inputEmail3" class="col-sm-4 control-label">Adhérent</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="inputEmail3" name="login" value="<?php if(isset($_POST['login'])){echo htmlentities($_POST['login'], ENT_QUOTES, 'UTF-8');} ?>" />
				</div>				
			</div>

			<button class="btn btn-danger pull-right">Valider</button>


		</div>
	</form>
	<?php

?>
</div>
</div>
</div>
</div>
</div>

<?php include 'footer.php';?> 