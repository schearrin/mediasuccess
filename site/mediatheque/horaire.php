<?php include 'header.php';?>
<div class="container">

	<!-- form -->
	<div class="content inside-page contact">
		<div class="breadcrumb"><a href="index.php">Home</a> / Horaires</div>
		<h2 class="title">Horaires</h2>


		<div class="row">

			<div class="col-sm-12">
				<div class="map">
					<div class="image_wrapper"><p style="text-align:center"><img src="./images/horaire.gif" width="80%" height="300"></p></div>
				</div>


				<div class="location col-sm-6 col-sm-offset-3">
					<h4>Nos horaires</h4>
					<p>La mediatheque ouvre ses portes du lundi au vendredi de 10h a 20h.</p>
				</div>
			</div>
			
		</div>


	</div>
	<!-- form -->


</div>
<?php include 'footer.php';?>