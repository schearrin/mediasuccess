<?php

// on verifie que les logins sont uniques : retourne false si unique
function verif_uniqueloginadherent($login){
	require 'connect_base.php';

	$select = 'select * from adherent where loginA="'.$login.'"';
	$res = mysqli_query($connexionbdd, $select)
	or die ("erreur de requête : " . $res);
	if (mysqli_num_rows($res)==0){
		return false;
	}
	return true;
}

function verif_uniquelogingestionnaire($login){
	require 'connect_base.php';

	$select = 'select * from gestionnaire where loginG="'.$login.'"';
	$res = mysqli_query($connexionbdd, $select)
	or die ("erreur de requête : " . $res);
	if (mysqli_num_rows($res)==0){
		return false;
	}
	return true;
}

// on verifie que les adresses mails sont uniques dans la table adherent : retourne true si unique
function verif_mailunique($email){
	require 'connect_base.php';

	$select = 'select email from adherent where email ="'.$email.'"';
	$res = mysqli_query($connexionbdd, $select)
	or die ("erreur de requête : ".$res);
	if (mysqli_num_rows($res)==0){
		return true;
	}
	return false;
}

// recupere les medias sous formes
function get_alloeuvres($typeoeuvre, $typetypeoeuvre){
	require 'connect_base.php';

	if($typeoeuvre == 'livre'){
		$select = "select * from oeuvre as OEU, livre as LIV where OEU.idO = LIV.idL and LIV.typeL ='".$typetypeoeuvre."'";
	}
	if($typeoeuvre == 'musique'){
		$select = 'select * from oeuvre as OEU, musique as MUS where OEU.idO = MUS.idM and MUS.typeM ="'.$typetypeoeuvre.'"';
	}
	if($typeoeuvre == 'film'){
		$select = 'select * from oeuvre as OEU, film as FIL where OEU.idO = FIL.idF and FIL.typeF ="'.$typetypeoeuvre.'"';
	}
	$req = sprintf($select, $typetypeoeuvre);
	$res = mysqli_query($connexionbdd, $req)
	or die ("erreur de requête : " . $req);
	if(mysqli_num_rows($res)==0){
		$tab = [];
	}
	if (mysqli_num_rows($res)<>0){
		while ($e = mysqli_fetch_assoc($res) and isset($e)) {
			$tab[] = $e;
		}	
	}
	$taille = count($tab);
	return $tab;
}

// recuperer information d'une oeuvre precise a partir de son identifiant
function get_oeuvre($id){
	require 'connect_base.php';
	$tab = array();
	$select = 'select * from oeuvre where idO='.$id;
	$res = mysqli_query($connexionbdd, $select)
	or die ("erreur de requête : ". $res);
	if (mysqli_num_rows($res) <> 0){
		while ($e = mysqli_fetch_assoc($res) and isset($e)) {
			$tab[] = $e;
		}	
	}
	return $tab;
}

// recupere description pour pour un id oeuvre precis
function get_description($id){
	require 'connect_base.php';
	$tab = array();
	$select = 'select * from description where idO='.$id;
	$res = mysqli_query($connexionbdd, $select)
	or die ("erreur de requête : ". $res);
	if (mysqli_num_rows($res) <> 0){
		$result = mysqli_fetch_assoc($res);
		$tab = $result['synopsis'];
	}
	if (mysqli_num_rows($res) == 0){
		$tab = 'Pas de résumé disponible';
	}
	return $tab;
}

// recupere les exemplaires a partir de l'idO
function get_exemplaire($id){
	require 'connect_base.php';
	$tab = array();
	$select = 'select * from exemplaire where idO='.$id;
	$res = mysqli_query($connexionbdd, $select)
	or die ("erreur de requête : ". $res);
	if(mysqli_num_rows($res)==0){
		$tab = [];
	}
	if (mysqli_num_rows($res) <> 0){
		while ($e = mysqli_fetch_assoc($res) and isset($e)) {
			$tab[] = $e;
		}	
	}
	return $tab;
}

// recupere le nombre total d'exemplaire disponible a partir de l'idO
function get_nbtotalexemplairedispo($id){
	require 'connect_base.php';
	$select = 'select count(*) from exemplaire where etat = "disponible" and idO='.$id;
	$res = mysqli_query($connexionbdd, $select)
	or die ("erreur de requête : ". $res);
	$tab = mysqli_fetch_assoc($res);
	return $tab['count(*)'];
}

// tout les exemplaires qui depasse les 48h ou les 3 jours sont mis a jour et redeviennent disponible
function maj_48hreserver(){
	require 'connect_base.php';

	//recupere la date d'aujourd'hui
	$selectdatenow = 'select now()';
	$resnow = mysqli_query($connexionbdd, $selectdatenow)
	or die ("erreur de requête : ". $resnow);
	$tabnow = mysqli_fetch_assoc($resnow);

	//recupere le tableau des dates de reservation
	$selectDRES = 'select idE, dateRes from reservation';
	$resDRES = mysqli_query($connexionbdd, $selectDRES)
	or die ("erreur de requête : ". $resDRES);	
	$tabDRES = array();
	if(mysqli_num_rows($resDRES) <> 0){
		while ($e = mysqli_fetch_assoc($resDRES) and isset($e)) {
			$tabDRES[] = $e;
		}	
	}

	//pour chaque ligne, fait la difference entre les 2 dates : si depassement alors redevient disponible
	foreach ($tabDRES as $ligne) {

		$selectdiff = 'select DATEDIFF("'.$tabnow['now()'].'","'.$ligne['dateRes'].'")';
		$req = sprintf($selectdiff, $tabnow['now()'], $ligne['dateRes']);
		$resdiff = mysqli_query($connexionbdd, $req)
		or die ("erreur de requête :". $req);
		$tabdiff = mysqli_fetch_assoc($resdiff);
		$valeurtabdiff = $tabdiff['DATEDIFF("'.$tabnow['now()'].'","'.$ligne['dateRes'].'")'];

		if($valeurtabdiff >= 3){
			$delete = 'delete from reservation where idE ='.$ligne['idE'];
			$update = 'update exemplaire set etat = "disponible" where idE='.$ligne['idE'];
			$execdelete = mysqli_query($connexionbdd, $delete) or die ("erreur de requête delete : ". $execdelete);
			$execupdate = mysqli_query($connexionbdd, $update) or die ("erreur de requête update : ". $execupdate);
		}
	}
}

// pour le gestionnaire : recupere l'ido a partir de l'ide
function get_idoeuvre($idexemplaire){
	require 'connect_base.php';

	$select = 'select idO from exemplaire where idE='.$idexemplaire;
	$req = sprintf($select, $idexemplaire);
	$res = mysqli_query($connexionbdd, $req);
	$tab = mysqli_fetch_assoc($res);
	
	return $tab['idO'];
}

// pour l'adherent : verifie si des exemplaires sont disponibles
function verif_exemplairedispo($idoeuvre){
	maj_48hreserver();

	require 'connect_base.php';
	$select = 'select * from exemplaire where etat= "disponible" and idO='.$idoeuvre;
	$res = mysqli_query($connexionbdd, $select)
	or die ("erreur de requête : ".$res);
	if(mysqli_num_rows($res)==0){
		return false;
	}
	return true;
}

//verifie que l'adherent existe
function verif_adherent($logadherent){
	require 'connect_base.php';

	$select = 'select * from adherent where loginA= "'.$logadherent.'"';
	$res = mysqli_query($connexionbdd, $select)
	or die ("erreur de requête select adherent : ".$res);
	if(mysqli_num_rows($res) <>0){
		return true;
	}
	return false;
}

//pour le gestionnaire : verifie si l'exemplaire est disponible et que l'adherent existe aussi
function verif_exemplairedispo2($idexemplaire, $idadherent){
	maj_48hreserver();

	require 'connect_base.php';

	$select = 'select * from exemplaire where etat= "disponible" and idE='.$idexemplaire;
	$selectR = 'select * from reservation where idE='.$idexemplaire.' and idA='.$idadherent;

	$res = mysqli_query($connexionbdd, $select)	or die ("erreur de requête : ".$res);
	$resR = mysqli_query($connexionbdd, $selectR) or die ("erreur de requête : ".$resR);

	if(mysqli_num_rows($res)<>0 || mysqli_num_rows($resR)<>0){
		return true;
	}
	return false;
}

//adherent (non bdd) : verifie s'il existe des retards dans la table emprunt
function verif_sipasretard($idadherent){
	//recup de la liste des emprunts :
	$tabemprunt = listeemprunt($idadherent);

	foreach ($tabemprunt as $ligne) {
		if(diff_datenowretour($ligne['idE']) > 0){
			return false;
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//gestionnaire  chercher le login adherent correspondant a l'identifiant adherent
function cherche_logadher($idAdhe){
	require 'connect_base.php';
	
	$select = 'select idA from adherent where loginA ="'.$idAdhe.'"';
	$req = sprintf($select, $idAdhe);
	$res = mysqli_query($connexionbdd, $req);
	$table = mysqli_fetch_assoc($res);
	
	return $table['idA'];
}

//gestionnaire : verifie si l'exemplaire existe dans la table reservation
function verif_existinreservation($idexemplaire, $idadherent){
	require 'connect_base.php';

	$select = 'select * from reservation where idA='.$idadherent.' and idE='.$idexemplaire;
	$req = sprintf($select, $idexemplaire, $idadherent);
	$res = mysqli_query($connexionbdd, $req)
	or die("erreur de requête :".$req);
	if (mysqli_num_rows($res)==0){
		return false;
	}
	return true;
}

//gestionnaire : ajouter un exemplaire dans la table achat 
function achat($idex , $loginadh){
	require 'connect_base.php';

	$idAdhe = cherche_logadher($loginadh);

	if(verif_exemplairedispo2($idex, $idAdhe) && verif_sipasretard($idAdhe)){

		$insert = 'insert into achat values ('.$idAdhe. ','.$idex.',now())'; 
		$req = sprintf($insert, $idAdhe, $idex);
		mysqli_query($connexionbdd, $req) or die ("erreur de requête : ". $req);

		$update = 'update exemplaire set etat ="achete" where idE='.$idex;
		$req = sprintf($update, $idex);
		mysqli_query($connexionbdd, $req) or die ("erreur de requête : ". $req);

		if(verif_existinreservation($idex, $idAdhe)){
			$idoeuvre = get_idoeuvre($idex);
			faire_annulerreservation($idAdhe, $idoeuvre);
		}
		
		return true;
	}
	return false;
}

//gestionnaire : limite emprunt de l'adherent
function limiteemprunt($idadherent){
	require 'connect_base.php';

	$select = 'select count(*) from emprunt where idA='.$idadherent;
	$res = mysqli_query($connexionbdd, $select)
	or die ("erreur de requête select dans la fonction limitenondepasse :". $req);
	$result = mysqli_fetch_assoc($res);
	if ($result['count(*)'] <10){
		return true;
	}else{
		return false;
	}
}


//gestionnaire : enregistre emprunt d'un exemplaire si disponible
function emprunt($idexemplaire, $logadherent){
	require 'connect_base.php';

	$idadherent = cherche_logadher($logadherent);
	if(verif_exemplairedispo2($idexemplaire, $idadherent) && limiteemprunt($idadherent) && verif_sipasretard($idadherent)){

		$update = 'update exemplaire set etat = "emprunte" where idE = '.$idexemplaire;
		mysqli_query($connexionbdd, $update);

		$select= 'insert into emprunt(idA,idE,dateEmp,dateRet) values('.$idadherent.','.$idexemplaire.', now(), now()+ interval 21 day)';
		
		if(verif_existinreservation($idexemplaire, $idadherent)){
			$idoeuvre = get_idoeuvre($idexemplaire);
			faire_annulerreservation($idadherent, $idoeuvre);
		}

		//s'il a reussi a inserer alors return true
		if(mysqli_query($connexionbdd,$select)){
			return true;
		}else{
			//sinon on garde l'etat disponible
			$select= 'update exemplaire set etat="disponible" where idE = '.$idexemplaire;
			return false;
		}
	}
	else{
		return false;
	}
}

//gestionnaire : verifie si l'exemplaire existe dans la table emprunt, renvoie true si existe
function verif_ifexistinemprunt($idexemplaire, $idadherent){
	require 'connect_base.php';

	$select = 'select * from emprunt where idA='.$idadherent.' and idE='.$idexemplaire;
	$req = sprintf($select, $idexemplaire, $idadherent);
	$res = mysqli_query($connexionbdd, $req)
	or die("erreur de requête :".$req);
	if (mysqli_num_rows($res)==0){
		return false;
	}
	return true;
}

// gestionnaire : fait la difference entre date ajourd'hui et retour à partir de l'id exemplaire : renvoie nb jours retard
function diff_datenowretour($idexemplaire){
	require 'connect_base.php';

	//recupere la date de retour
	$selectdateret = 'select dateRet from emprunt where idE='.$idexemplaire;
	$req = sprintf($selectdateret, $idexemplaire);
	$resdateret = mysqli_query($connexionbdd, $req)
	or die ("erreur de requête : ". $resdateret);
	$tabdateret = mysqli_fetch_assoc($resdateret);
	$valeurtab = $tabdateret['dateRet'];

	//calcul de la difference entre date now et date retour
	$selecttabdiff = 'select DATEDIFF( now(), "'.$valeurtab.'")';
	$req = sprintf($selecttabdiff, $valeurtab);
	$res = mysqli_query($connexionbdd, $req)
	or die ("erreur de requête :". $res);
	$tabdiff = mysqli_fetch_assoc($res);
	$nbjour = $tabdiff['DATEDIFF( now(), "'.$valeurtab.'")'];

	return $nbjour;
}

//gestionnaire : recupere le prix de de l'exemplaire
function get_prix($idexemplaire){
	require 'connect_base.php';

	$select = 'select O.prix from oeuvre as O, exemplaire as E where O.idO = E.idO and E.idE='.$idexemplaire;
	$req = sprintf($select, $idexemplaire);
	$res = mysqli_query($connexionbdd, $req)
	or die("erreur de requête : ".$req);
	$tab = mysqli_fetch_assoc($res);
	$valeurtab = $tab['prix'];

	return $valeurtab;
}

// gestionnaire : retourner un exemplaire cad supprimer un exemplaire de la table emprunt et changer l'etat de lexemplaire
function retour($idex , $loginadh){
	require 'connect_base.php';

	$idAdhe = cherche_logadher($loginadh);

	if(verif_ifexistinemprunt($idex, $idAdhe)){

		$delete = 'delete from emprunt where idE ='.$idex.' && idA =' .$idAdhe;
		$req = sprintf($delete, $idAdhe);
		$res1 = mysqli_query($connexionbdd, $req) or die ("erreur de requête : ". $req);

		$update = 'update exemplaire set etat="disponible" where idE = '.$idex;
		$req = sprintf($update, $idAdhe);
		$res2 = mysqli_query($connexionbdd, $req) or die ("erreur de requête : ". $req);

		return true;
	}else{
		return false;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//adherent : liste des emprunts en cours
function listeemprunt($idadherent){

	require 'connect_base.php';

	$tab = array();
	$select = 'select * from emprunt as EMPRU, exemplaire as EXEMP, oeuvre as OEUV where EMPRU.idE = EXEMP.idE and OEUV.idO = EXEMP.idO and EMPRU.idA ='.$idadherent;
	$res = mysqli_query($connexionbdd, $select)
	or die("erreur de requête : ".$res);
	if (mysqli_num_rows($res) <> 0){
		while ($e = mysqli_fetch_assoc($res) and isset($e)) {
			$tab[] = $e;
		}	
	}
	$taille = count($tab);
	return $tab;
}

//adherent : historique de ses achats
function historiqueachat($idadherent){
	require 'connect_base.php';

	$tab = array();
	$select = 'select * from achat as ACH, exemplaire as EXEMP, oeuvre as OEUV where ACH.idE = EXEMP.idE and OEUV.idO = EXEMP.idO and ACH.idA ='.$idadherent;
	$res = mysqli_query($connexionbdd, $select)
	or die("erreur de requête : ".$res);
	if (mysqli_num_rows($res) <> 0){
		while ($e = mysqli_fetch_assoc($res) and isset($e)) {
			$tab[] = $e;
		}	
	}
	$taille = count($tab);
	return $tab;
}

//adherent : verifie si exemplaire existe deja dans la table reservation, renvoie vrai si existe
function verif_siexistinreservation($idoeuvre, $idadherent){
	require 'connect_base.php';

	$select = 'select * from reservation as RESERV, exemplaire as EXEMP where EXEMP.idE= RESERV.idE and EXEMP.idO ='.$idoeuvre.' and RESERV.idA='.$idadherent;
	$req = sprintf($select, $idoeuvre, $idadherent);
	$res = mysqli_query($connexionbdd, $req)
	or die("erreur de requête :".$req);
	if (mysqli_num_rows($res)==0){
		return false;
	}
	return true;
}

//adherent : fonction qui renvoie l'identifiant du premier exemplaire qu'il voit de disponible
function get_idexemplaire($idoeuvre){
	require 'connect_base.php';

	$tab = array();
	$select = 'select idE from exemplaire where idO='.$idoeuvre.' and etat= "disponible"';
	$req = sprintf($select, $idoeuvre);
	$res = mysqli_query($connexionbdd, $req)
	or die ("erreur de requête select : ". $req);
	if (mysqli_num_rows($res) <> 0){
		while ($e = mysqli_fetch_assoc($res) and isset($e)) {
			$tab[] = $e;
		}	
	}
	return $tab[0]['idE'];
}

//adherent : limite de reservation
function verif_limitereservation($idadherent){
	require 'connect_base.php';

	$select = 'select count(*) from reservation where idA='.$idadherent;
	$res = mysqli_query($connexionbdd, $select)
	or die ("erreur de requête select dans la fonction limitenondepasse :". $req);
	$result = mysqli_fetch_assoc($res);
	if ($result['count(*)'] <10){
		return true;
	}else{
		return false;
	}
}

//adherent : renvoie true si reservation fait
function faire_reservation($idadherent, $idoeuvre){
	require 'connect_base.php';

	if(verif_exemplairedispo($idoeuvre) && verif_limitereservation($idadherent)){
		$idexemplaire = get_idexemplaire($idoeuvre);
		$insert = 'insert into reservation values ('.$idadherent.','.$idexemplaire.',now())';
		$update = 'update exemplaire set etat= "reserve" where idE='.$idexemplaire;
		$execinsert = mysqli_query($connexionbdd, $insert) or die ("erreur de requête delete : ". $execinsert);
		$execupdate = mysqli_query($connexionbdd, $update) or die ("erreur de requête delete : ". $execupdate);

		return true;
	}else{
		return false;
	}

}

//adherent : annuler reservation
function faire_annulerreservation($idadherent, $idoeuvre){
	require 'connect_base.php';

	//recherche ide
	$selectide = 'select * from exemplaire as EXEMP, reservation as RESERV where EXEMP.idE = RESERV.idE and idA='.$idadherent.' and EXEMP.idO='.$idoeuvre;
	$req = sprintf($selectide, $idadherent, $idoeuvre);
	$res = mysqli_query($connexionbdd, $req)
	or die("erreur de requête : ".$req);

	if (mysqli_num_rows($res) <> 0){
		$result = mysqli_fetch_assoc($res);
		$idexemplaire = $result['idE'];

		//supprime de la table reservation et met a jour la table oeuvre
		$delete = 'delete from reservation where ida='.$idadherent.' and idE='.$idexemplaire;
		$update = 'update exemplaire set etat ="disponible" where idE='.$idexemplaire;

		$execupdate = mysqli_query($connexionbdd, $update) or die ("erreur de requête delete : ". $execupdate);
		$execdelete = mysqli_query($connexionbdd, $delete) or die ("erreur de requête delete : ". $execdelete);;
	}
}

//adherent : liste de reservation en cours pour les livres
function listereservation($idadherent){
	require 'connect_base.php';

	maj_48hreserver();

	$tab = array();

	$selectforliv = '( select R.idE, R.dateRes, E.idO, L.typeL, O.titre, O.auteur from reservation as R, oeuvre as O, exemplaire as E, livre as L where idA='.$idadherent.' and R.idE=E.idE and E.idO=O.idO and O.idO = L.idL )';
	$selectformus = '( select R.idE, R.dateRes, E.idO, M.typeM, O.titre, O.auteur from reservation as R, oeuvre as O, exemplaire as E, musique as M where idA='.$idadherent.' and R.idE=E.idE and E.idO=O.idO and O.idO = M.idM )';
	$selectforfil = '( select R.idE, R.dateRes, E.idO, F.typeF, O.titre, O.auteur from reservation as R, oeuvre as O, exemplaire as E, film as F where idA='.$idadherent.' and R.idE=E.idE and E.idO=O.idO and O.idO = F.idF )';
	$union = 'select T1.idO, T1.idE, T1.titre, T1.auteur, T1.dateRes, T1.typeL from '.$selectforliv.' as T1 union all select T2.idO, T2.idE, T2.titre, T2.auteur, T2.dateRes, T2.typeM from '.$selectformus.' as T2 union all select T3.idO, T3.idE, T3.titre, T3.auteur, T3.dateRes, T3.typeF from '.$selectforfil.' as T3';

	$req = sprintf($union, $idadherent);
	$res = mysqli_query($connexionbdd, $req)
	or die ("erreur de requête select : ". $req);
	if (mysqli_num_rows($res) <> 0){
		while ($e = mysqli_fetch_assoc($res) and isset($e)) {
			$tab[] = $e;
		}	
	}

	return $tab;
}


//adherent : demande de prolongation
function add_prolongation($idadherent, $idexemplaire){
	require 'connect_base.php';

	//recupere date retour et verifie que la prolongation n'est pas deja faite
	$select = 'select dateRet from emprunt where idA='.$idadherent.' and idE='.$idexemplaire.' and prolongation= "false"';
	
	$req = sprintf($select, $idadherent, $idexemplaire);
	$res = mysqli_query($connexionbdd, $select)
	or die("erreur de requête :".$req);

	if (mysqli_num_rows($res) <> 0){
			$result = mysqli_fetch_assoc($res);
			$dateretour = $result['dateRet'];
		//met a jour
			$update = 'update emprunt set prolongation="true", dateRet="'.$dateretour.'" + interval 21 day where idE='.$idexemplaire;
			$execupdate = mysqli_query($connexionbdd, $update) or die ("erreur de requête delete : ". $execupdate);
			
			return true;
		}
		return false;
}


?>