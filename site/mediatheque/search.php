<?php include 'header.php';?>

<div class="container">
   <div class="content inside-page collection">
      <div class="breadcrumb"><a href="index.php">Home</a> / Rechercher </div>
      <h2 class="title">Résultat de votre recherche :</h2>
      <div class="row">

        <?php

        if(isset($_POST['Recherche'])){

    //union des tables oeuvre, livre, musique et film
         $selectforliv = '( select O.idO, O.titre, O.auteur, O.prix, O.nom_img, E.idE, L.typeL from oeuvre as O, livre as L, exemplaire as E where E.idO=O.idO and O.idO = L.idL )';
         $selectformus = '( select O.idO, O.titre, O.auteur, O.prix, O.nom_img, E.idE, M.typeM from oeuvre as O, musique as M, exemplaire as E where E.idO=O.idO and O.idO = M.idM )';
         $selectforfil = '( select O.idO, O.titre, O.auteur, O.prix, O.nom_img, E.idE, F.typeF from oeuvre as O, film as F, exemplaire as E where E.idO=O.idO and O.idO = F.idF )';
         $union = '( select T1.idO, T1.titre, T1.auteur, T1.prix, T1.nom_img, T1.typeL from '.$selectforliv.' as T1 union select T2.idO, T2.titre, T2.auteur, T2.prix, T2.nom_img, T2.typeM from '.$selectformus.' as T2 union select T3.idO, T3.titre, T3.auteur, T3.prix, T3.nom_img, T3.typeF from '.$selectforfil.' as T3 )';


         $_POST['Recherche'] = mysqli_real_escape_string($connexionbdd, $_POST['Recherche']);
         $recherche = $_POST['Recherche'];


         $requete = 'select * from '.$union.' as U where U.titre LIKE "%'.$recherche.'%" OR U.auteur LIKE "%'.$recherche.'%" OR U.typeL LIKE "%'.$recherche.'%"';

         $result = mysqli_query($connexionbdd, $requete)
         or die("erreur de requête :".$result);
         if (mysqli_num_rows($result) <> 0){
            while ($e = mysqli_fetch_assoc($result) and isset($e)) {
                $tab[] = $e;
            }

        //on affiche les oeuvres
            foreach ($tab as $ligne) {

            //si livre
                if( ($ligne['typeL']=='BD') || ($ligne['typeL']=='Journaux') || ($ligne['typeL']=='Magazines') || ($ligne['typeL']=='Romans')){
                 echo '<div class="col-sm-4">';
                 echo '<div class="product">';
                 echo '<a href="#">';
                 echo '<img src="images/products/livre/'.$ligne['typeL'].'/'.$ligne['nom_img'].'.jpg" class="img-responsive"  alt="product">';
                 echo '<span class="price special">'.$ligne['prix'].' €'.'</span>';                                 
                 echo '</a>';

                 echo '<div class="overlay">';
                 echo '<div class="detail">';
                 echo '<h4><a href="#">'.$ligne['titre'].'</a></h4>';
                 echo '<a href="product.php?idoeuvre='.$ligne['idO'].'&typeoeuvre=livre&typetypeoeuvre='.$ligne['typeL'].'" class="btn btn-default view animated fadeInLeft"><i class="fa fa-search-plus"></i> Information</a>';                              
                 echo '</div>';
                 echo '</div>';
                 echo '</div>';
                 echo '</div>';
             }

           //si musique
             if( ($ligne['typeL']=='Electro') || ($ligne['typeL']=='Pop') || ($ligne['typeL']=='Rap') || ($ligne['typeL']=='Rnb') || ($ligne['typeL']=='Rock')){
               echo '<div class="col-sm-4">';
               echo '<div class="product">';
               echo '<a href="#">';
               echo '<img src="images/products/musique/'.$ligne['typeL'].'/'.$ligne['nom_img'].'.jpg" class="img-responsive"  alt="product">';
               echo '<span class="price special">'.$ligne['prix'].' €'.'</span>';                                 
               echo '</a>';

               echo '<div class="overlay">';
               echo '<div class="detail">';
               echo '<h4><a href="#">'.$ligne['titre'].'</a></h4>';
               echo '<a href="product.php?idoeuvre='.$ligne['idO'].'&typeoeuvre=musique&typetypeoeuvre='.$ligne['typeL'].'" class="btn btn-default view animated fadeInLeft"><i class="fa fa-search-plus"></i> Information</a>';                              
               echo '</div>';
               echo '</div>';
               echo '</div>';
               echo '</div>';
           }

           //si film
           if( ($ligne['typeL']=='Action') || ($ligne['typeL']=='Comedie') || ($ligne['typeL']=='Horreur') || ($ligne['typeL']=='Policier') || ($ligne['typeL']=='Romance')){
               echo '<div class="col-sm-4">';
               echo '<div class="product">';
               echo '<a href="#">';
               echo '<img src="images/products/film/'.$ligne['typeL'].'/'.$ligne['nom_img'].'.jpg" class="img-responsive"  alt="product">';
               echo '<span class="price special">'.$ligne['prix'].' €'.'</span>';                                 
               echo '</a>';

               echo '<div class="overlay">';
               echo '<div class="detail">';
               echo '<h4><a href="#">'.$ligne['titre'].'</a></h4>';
               echo '<a href="product.php?idoeuvre='.$ligne['idO'].'&typeoeuvre=film&typetypeoeuvre='.$ligne['typeL'].'" class="btn btn-default view animated fadeInLeft"><i class="fa fa-search-plus"></i> Information</a>';                              
               echo '</div>';
               echo '</div>';
               echo '</div>';
               echo '</div>';
           }

       }
   }

   if (mysqli_num_rows($result)==0){
    echo "<h4>Aucun résultat ne correspond à votre recherche.</h4>";
}
}

?>
</div>
</div>
</div>
<?php include 'footer.php';?>