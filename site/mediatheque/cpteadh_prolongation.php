<?php include 'header.php';?>
<div class="container">
  <div class="content inside-page about">
    <div class="breadcrumb"><a href="index.php">Home</a> / Mon compte</div>

    <!-- titre -->
    <h2 class="title">Mon compte</h2>

    <div class="row">
      <!-- ce qui se trouve sur la gauche -->
    <div class="col-sm-4">
      <h3>Profil</h3>
      <ul class="compte-menu-vertical">
        <li class="compte-mv-item"><a href="cpteadh_informations.php">Informations</a></li>
      </ul>
      <br /><br />

      <h3>Menu</h3>
      <ul class="compte-menu-vertical">
        <li class="compte-mv-item"><a href="cpteadh_reservation.php">Liste des réservations</a></li>
        <li class="compte-mv-item"><a href="cpteadh_prolongation.php">Liste des emprunts</a></li>
        <li class="compte-mv-item"><a href="cpteadh_achat.php">Historique d'achat</a></li>
      </ul>
      <br /><br />
    </div>
      <!-- ce qui se trouve sur la droite -->  	
      <div class="col-sm-8">


        <h3>Liste des emprunts</h3>
        <br />
        <?php $tab = listeemprunt($_SESSION['userid']); ?>
        <div class="location col-sm-13 col-sm-offset-1">
          <table class="compte-menu-vertical" style="font-size:125%;">
            <tr>
              <td><b>Id</b></td>
              <td><b>Titre</b></td>
              <td><b>Date d'emprunt</b></td>
              <td><b>Date de retour</b></td>
            </tr>
            <?php
            foreach ($tab as $ligne) {
              echo '<tr>';
              echo '<td>'.$ligne['idE'].'</td>';
              echo '<td>'.$ligne['titre'].'</td>';
              echo '<td>'.$ligne['dateEmp'].'</td>';
              echo '<td>'.$ligne['dateRet'].'</td>';
              echo '</tr>';
            }
           ?>
          </table>
       </div>
       <br /><br />
       <h4>Demander une prolongation (saisir Id de l'exemplaire) : </h4>

       <?php
       if(isset($_SESSION['logadherent']) && isset($_POST['idoeuvre'])){

         $_POST['idoeuvre'] = mysqli_real_escape_string($connexionbdd, $_POST['idoeuvre']);

         if(add_prolongation($_SESSION['userid'], $_POST['idoeuvre'])){
          echo '<META HTTP-EQUIV="Refresh" CONTENT="0;URL= cpteadh_prolongation.php" />';
        }else{
          echo "<p>L'Id exemplaire n'est pas correct ou vous avez déjà fait une demande de prolongation.</p>";
        }
      }
      ?>
        <form class="form-horizontal col-sm-10 col-sm-offset-1" method="post" action="cpteadh_prolongation.php">
    <div class="panel-body">

      <div class="form-group">
        <label for="inputEmail3" class="col-sm-4 control-label">Id Exemplaire</label>
        <div class="col-sm-8">
          <input type="text" class="form-control" id="inputEmail3" name="idoeuvre" value="<?php if(isset($_POST['idoeuvre'])){echo htmlentities($_POST['idoeuvre'], ENT_QUOTES, 'UTF-8');} ?>" />
        </div>        
      </div>

      <button class="btn btn-danger pull-right">Valider</button>


    </div>
  </form>

    </div>
  </div>


</div>
</div>
</div>
<?php include 'footer.php';?>