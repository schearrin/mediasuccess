<?php include 'header.php';?>
<div class="container">

<!-- form -->
<div class="content inside-page contact">
		<div class="breadcrumb"><a href="index.php">Home</a> / Contact</div>
       <h2 class="title">Contact</h2>


       <div class="row">
       	
       	<div class="col-sm-12">
       	<div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2632.7477061014515!2d2.1730990000000068!3d48.71030300000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e67f4da135a5df%3A0x2b9d2ea38dcbf690!2sP.U.I.O.!5e0!3m2!1sfr!2sfr!4v1428305405807" width="100%" height="300" frameborder="0" style="border:0"></iframe>
       	</div>


		<div class="location col-sm-6 col-sm-offset-3">
       		<h4>Mediatheque</h4>
       		<p>15 Rue Georges Clemenceau, 91400 Orsay <br>
       		Bâtiment 640 PUIO<br>
       		Téléphone : 01 69 15 67 50<br>
       		contact@projetGLAL3MIAGE-mediatheque.com
       		</p>

       		<h4>Nous écrire</h4>
			<form role="form">
			<div class="form-group">	
			<input type="text" class="form-control" id="name" placeholder="Nom">
			</div>
			<div class="form-group">
			<input type="email" class="form-control" id="email" placeholder="Adresse email">
			</div>
			<div class="form-group">
			<input type="phone" class="form-control" id="phone" placeholder="Téléphone">
			</div>
			<div class="form-group">
			<textarea type="email" class="form-control"  placeholder="Message" rows="4"></textarea>
			</div>
					
			<button type="submit" class="btn btn-primary">Envoyer</button>
			</form>


       	</div>





       </div>
</div>
</div>
<!-- form -->

</div>
<?php include 'footer.php';?>