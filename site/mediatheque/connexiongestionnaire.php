<?php include 'header.php';?> 

<div class="container">

  <div class="content inside-page create-account">
    <h2 class="title">Connexion</h2>
    <div class="row">

      <?php
  //deconnexion de l'utilisateur si connecte
      if(isset($_SESSION['loggestionnaire']))
      {
  //suppression des variables de la session courante
        session_unset();
        ?>
        <div class="form-horizontal col-sm-6 col-sm-offset-3">
          <div class="panel-body">
            <h4>Déconnexion réussie</h4>
            <br /><br />
            <div class="message">Vous avez bien &eacute;t&eacute; d&eacute;connect&eacute;.<br /></div>
            <br /><br />
          </div>
        </div>
        <?php
      }
      else
      {
        $ologgestionnaire = '';
  //verification formulaire
        if(isset($_POST['loggestionnaire'], $_POST['motdepasse']))
        {
    //echappe les variables pour pouvoir les mettre dans des requetes SQL
          if(get_magic_quotes_gpc())
          {
            $ologgestionnaire = $_POST['loggestionnaire'];
            $loggestionnaire = $_POST['loggestionnaire'];
            $motdepasse = stripslashes($_POST['motdepasse']);
          }
          else
          {
            $loggestionnaire = $_POST['loggestionnaire'];
            $motdepasse = $_POST['motdepasse'];
          }
    //recuperation du mot de passe de l'utilisateur
          $req = mysqli_query($connexionbdd, 'select mdp, loginG, idG, nom, prenom from gestionnaire where loginG="'.$loggestionnaire.'"');
          $dn = mysqli_fetch_array($req);

    //compare les mots de passe chiffrer
          $motdepasse = sha1($motdepasse);
          if($dn['mdp']==$motdepasse)
          {
      //n'affiche pas de message si mot de passe correct
            $form = false;
      //enregistre les informations dans la session
            $_SESSION['loggestionnaire'] = $_POST['loggestionnaire'];
            $_SESSION['userid'] = $dn['idG'];
            $_SESSION['gestionnairenom'] = $dn['nom'];
            $_SESSION['gestionnaireprenom'] = $dn['prenom'];
            ?>
            <div class="form-horizontal col-sm-6 col-sm-offset-3">
              <div class="panel-body">
                <h4>Connexion réussie</h4>
                <br /><br />
                <div class="message">Vous avez bien &eacute;t&eacute; connect&eacute;. Vous allez &ecirc;tre rediriger vers votre interface d'administration.<br /></div>
                <br /><br />
              </div>
            </div>
            <META HTTP-EQUIV="Refresh" CONTENT="3;URL= cptegest_informations.php" /> 
              <?php
            }
            else
            {
              $form = true;
              $message = 'La combinaison que vous avez entr&eacute; n\'est pas bonne.';
            }
          }
          else
          {
            $form = true;
          }


          if($form)
          {
            if(isset($message))
            {
              echo '<div class="message">'.$message.'</div>';
            }
            ?>

            <form class="form-horizontal col-sm-6 col-sm-offset-3" method="post" action="connexiongestionnaire.php">
              <div class="panel-body">

                <h4>Connexion gestionnaire :</h4>
                <br /><br />

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Identifiant</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputEmail3" name="loggestionnaire" value="<?php if(isset($_POST['loggestionnaire'])){echo htmlentities($_POST['loggestionnaire'], ENT_QUOTES, 'UTF-8');} ?>" />
                  </div>        
                </div>  


                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-4 control-label">Mot de passe</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="inputEmail3" name="motdepasse">
                  </div>        
                </div>
                <br />
                <div class="forgot-password">
                  <a href="connexion.php">Connexion adhérent ?</a>
                </div>


                <button class="btn btn-danger pull-right">Valider</button>

              </div>
            </form>

            <?php
          }
        }
        ?>
      </div>
    </div>
</div>

<?php include 'footer.php';?>