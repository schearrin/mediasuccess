<?php include 'header.php';?> 


<div class="container">
	<!-- form -->
	<div class="content inside-page create-account about">
		<div class="breadcrumb"><a href="index.php">Home</a> / Développeur</div>
		<h2 class="title">Développeur</h2>

		
		<div class="row">
			<!-- Menu de gauche -->
			<div class="col-sm-4">
				<h3>Profil</h3>
				<ul class="compte-menu-vertical">
					<li class="compte-mv-item"><a href="cptezadm_informations.php">Informations</a></li>
				</ul>
				<br /><br />

				<h3>Menu</h3>
				<ul class="compte-menu-vertical">
      <ul class="compte-menu-vertical">
        <li class="compte-mv-item"><a href="cptezadm_createaccountgest.php">Créer Gestionnaire</a></li>
      </ul>
			</div>


			<!-- Affichage droite -->
			<div class="col-sm-8">
				<h3>Créer un compte gestionnaire</h3><br /><br />

				<?php
//1 verification formulaire
				if(isset($_POST['nom'], $_POST['prenom'], $_POST['motdepasse'], $_POST['motdepasseverification']) and $_POST['nom']!='' and $_POST['prenom']!='')
				{
				//7 verification du nom et prenom
					if(preg_match('#^[a-zA-Z]+((-| )?[a-zA-Z]+)?$#', $_POST['nom']) && preg_match('#^[a-zA-Z]+((-| )?[a-zA-Z]+)?$#', $_POST['prenom'])) {

	//2 enleve lechappement si get_magic_quotes_gpc est active
						if(get_magic_quotes_gpc())
						{
							$_POST['nom'] = stripslashes($_POST['nom']);
							$_POST['prenom'] = stripslashes($_POST['prenom']);
							$_POST['motdepasse'] = stripslashes($_POST['motdepasse']);
							$_POST['motdepasseverification'] = stripslashes($_POST['motdepasseverification']);
						}


	//3 verification mot de passe identique
						if($_POST['motdepasse']==$_POST['motdepasseverification'])
						{
		//4 verification nombre de caracteres dans mot de passe
							if(strlen($_POST['motdepasse'])>=6)
							{

								$nom = $_POST['nom'];
								$prenom = $_POST['prenom'];
								$motdepasse = sha1($_POST['motdepasse']);

								$temp = time();
								$temp2 = 'gestionnaire';
								$loggestionnaire = generelogin($temp2, $prenom, $nom, $temp);

					//6 enregistre dans la base de donnee
								if(mysqli_query($connexionbdd, 'insert into gestionnaire (loginG, nom, prenom, mdp) values ("'.$loggestionnaire.'","'.$nom.'", "'.$prenom.'", "'.$motdepasse.'")'))
								{
									$form = false;
									?>
									<div class="message">
										Le nouveau gestionnaire a bien &eacute;t&eacute; inscrit. Il peut dor&eacute;navant se connecter.<br /><br />
										Son identifiant est le suivant :
										<?php
										echo "<h3>".$loggestionnaire."</h3>";

										mysqli_close($connexionbdd);
									}
									else
									{
										$form = true;
										$message = 'Une erreur est survenue lors de l\'inscription.';
									}

								}
								else
								{
									$form = true;
									$message = 'Le mot de passe que vous avez entr&eacute; contien moins de 6 caract&egrave;res.';
								}
							}
							else
							{
								$form = true;
								$message = 'Les mots de passe que vous avez entr&eacute; ne sont pas identiques.';
							}
						}else{
							$form = true;
							$message = 'Le nom ou prénom contiennent des caract&egraveres non autoris&eacutes';
						}
					}
					else
					{
						$form = true;
					}


					if($form)
					{
						if(isset($message))
						{
							echo '<div class="message">'.$message.'</div>';
						}
						?>

						<!-- formulaire -->
						<form class="form-horizontal col-sm-10 col-sm-offset-1" method="post" action="cptezadm_createaccountgest.php">
							<div class="panel-body">


								<div class="form-group">
									<label for="inputEmail3" class="col-sm-4 control-label">Nom</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="inputEmail3" name="nom" value="<?php if(isset($_POST['nom'])){echo htmlentities($_POST['nom'], ENT_QUOTES, 'UTF-8');} ?>" />
									</div>				
								</div>

								<div class="form-group">
									<label for="inputEmail3" class="col-sm-4 control-label">Prénom</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="inputEmail3" name="prenom" value="<?php if(isset($_POST['prenom'])){echo htmlentities($_POST['prenom'], ENT_QUOTES, 'UTF-8');} ?>" />
									</div>				
								</div>

								<div class="form-group">
									<label for="inputEmail3" class="col-sm-4 control-label">Mot de passe<span class="small">(6 caract&egrave;res min.)</span></label>
									<div class="col-sm-8">
										<input type="password" class="form-control" id="inputEmail3" name="motdepasse">
									</div>				
								</div>

								<div class="form-group">
									<label for="inputEmail3" class="col-sm-4 control-label">Re-entrer le mot de passe<span class="small">(v&eacute;rification)</span></label>
									<div class="col-sm-8">
										<input type="password" class="form-control" id="inputEmail3" name="motdepasseverification">
									</div>				
								</div>	


								<button class="btn btn-danger pull-right">Valider</button>


							</div>
						</form>

						<?php
					}
					?>
				</div>
			</div>
		</div>





	</div>
</div>
</div>


<?php include 'footer.php';?> 