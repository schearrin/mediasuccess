<?php include 'header.php';?>
<div class="container">


	<div class="content inside-page product-details">
		<?php
                //affiche oeuvre

		$tab = get_oeuvre($_GET['idoeuvre']);
		foreach ($tab as $ligne) { 
			echo '<div class="breadcrumb"><a href="index.php">Home</a> / <a href="collections.php?typeoeuvre='.$_GET['typeoeuvre'].'&typetypeoeuvre='.$_GET['typetypeoeuvre'].'">'.ucfirst($_GET['typeoeuvre']).'s </a> / '.$ligne['titre'].'</div>';
    echo '<div class="row">';
    echo '<div class="col-sm-5">';
    echo '<div class="item active"><img src="images/products/'.$_GET['typeoeuvre'].'/'.$_GET['typetypeoeuvre'].'/'.$ligne['nom_img'].'.jpg" class="img-responsive" alt="product"></div>';
    
    echo '</div>';
    echo '<div class="col-sm-6 col-sm-offset-1 information">';
    echo '<h3>'.$ligne['titre'].'</h3>';
    echo '<br />';
    echo '<h4> Auteur : '.$ligne['auteur'].'</h4>';
    echo '<br />';
    echo '<p style="font-size:130%"><b>Prix d\'emprunt : gratuit </b><br /><b>Prix d\'achat : '.$ligne['prix'].' € </b></p>';

    echo '<div class="clearfix">';
    echo '<br />';
    
    if(isset($_SESSION['logadherent'])){

    	$verifieexistance = verif_siexistinreservation($_GET['idoeuvre'], $_SESSION['userid']);

    	if($verifieexistance){

    		echo '<form class="form-horizontal" method="post" action="product.php?idoeuvre='.$ligne['idO'].'&typeoeuvre='.$_GET['typeoeuvre'].'&typetypeoeuvre='.$_GET['typetypeoeuvre'].'">';
    		echo '<div class="pull-left"><button name="reserver" value="non" class="btn btn-danger pull-right"><i class="fa fa-shopping-cart"> Annuler réservation</i></button></a></div>';
    		echo '</form>';

    		if(isset($_POST['reserver']) && $_POST['reserver'] =='non'){

    			faire_annulerreservation($_SESSION['userid'],$ligne['idO']);
                echo '<META HTTP-EQUIV="Refresh" CONTENT="0;URL= product.php?idoeuvre='.$ligne['idO'].'&typeoeuvre='.$_GET['typeoeuvre'].'&typetypeoeuvre='.$_GET['typetypeoeuvre'].'" />';

            }

    	}else{

    		echo '<form class="form-horizontal" method="post" action="product.php?idoeuvre='.$ligne['idO'].'&typeoeuvre='.$_GET['typeoeuvre'].'&typetypeoeuvre='.$_GET['typetypeoeuvre'].'">';
    		echo '<div class="pull-left"><button name="reserver" value="oui" class="btn btn-danger pull-right"><i class="fa fa-shopping-cart"> Réserver</i></button></a></div>';
    		echo '</form>';

            if(isset($_POST['reserver']) && $_POST['reserver'] =='oui'){
                if(faire_reservation($_SESSION['userid'],$ligne['idO'])){
                    if(verif_sipasretard($_SESSION['userid'])){
                    echo '<META HTTP-EQUIV="Refresh" CONTENT="0;URL= product.php?idoeuvre='.$ligne['idO'].'&typeoeuvre='.$_GET['typeoeuvre'].'&typetypeoeuvre='.$_GET['typetypeoeuvre'].'" />';
                    }else{
                    echo '<div class="pull-left">Vous n\'avez pas le droit de réserver.</div>';
                    }
                }else{
                    echo '<div class="pull-left">Vous avez dépassé la limite de réservation ou n\'avez pas le droit de réserver ou il n\'y a pas d\'exemplaire disponible.</div>';
                }
             }

         }
     }

    echo '</div>';

    echo '<div class="description-tabs">';
    echo '</div>';
}
            //affiche sa description
?>
<ul class="nav nav-tabs" role="tablist">
	<li class="active"><a href="#description" role="tab" data-toggle="tab">Résumé</a></li>
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="description">
		<p><br /><?php echo get_description($_GET['idoeuvre']); ?></p>
	</div>
</div>
</div>

</div>




		<div class="related-products">
			<h4>Exemplaires (disponible : <?php echo get_nbtotalexemplairedispo($_GET['idoeuvre']); ?>) </h4>
			<div class="row">
				<?php aff_exemplaire($_GET['idoeuvre']); ?>      
			</div>

		</div>



	</div>
</div>
<?php include 'footer.php';?>