﻿<?php 

session_start ();

if ((count($_GET)!=0) && !(isset($_GET['controle']) && isset ($_GET['action'])))
		require ('./vue/erreur404.tpl');	
else {
	
	if ((! isset($_SESSION['profilU'])) || count($_GET)==0)	{
		$controle = "utilisateur"; 
		if(!isset ($_GET['action'])) $_GET['action'] ="";
		switch($_GET['action']){
			default:
				$action="accueil";	
				break;
			case "ident":
				$action="ident";
				break;
			case "add":
				$action="add";
				break;
		}	
	}
	else {
		if (isset($_GET['controle']) && isset ($_GET['action'])) {
			$controle = $_GET['controle'];
			$action = 	 $_GET['action'];
		}
	}
	require ('./controle/' . $controle . '.php');
	$action ();
} 

?>
