<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Mon historique</title>

    <!-- Bootstrap core CSS -->
    <link href="./vue/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./vue/offcanvas.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php?controle=utilisateur&action=accueil">Catalogue</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
			<li><a href="index.php?controle=panier&action=detailPanier">Mon Panier</a></li>
			<li><a href="index.php?controle=historique&action=detailHistorique">Historique</a></li>
			<li><a href="index.php?controle=utilisateur&action=detailCompte">Compte</a></li>
            <li><a href="index.php?controle=utilisateur&action=deco">Déconnexion</a></li>
          </ul>
		  		                <SELECT name="Pays" size="1">
					<OPTION value="France" >Français</option>
					<OPTION value="Royaume-Uni">Anglais</option>
					<OPTION value="Allemagne">Allemand</option>
					<OPTION value="Italie">Italien</option>
					<OPTION value="Espagne">Espagnol</option>
					<OPTION value="Brésil">Portugais</option>
				</SELECT>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->

    <div class="container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
            <h2>Historique</h2>
			<br><br><br>
			<div class="responsive-table-line" style="margin:0px auto;max-width:700px;">
				<table class="table table-bordered table-condensed table-body-center" >
				<tr>
				   <td class="info"></td>
					<td class="info">Code</td>
					<td class="info">Produit</td>
					<td class="info">Quantité</td>
					<td class="info">Prix</td>
				</tr>
				
				<?php $i=1;
				foreach($histo as $Val){ ?>	
				<tr>
						<td class="info"><?php echo $i ?></td>
						<td ><?php echo $Val['cde_prod'] ?></td>
						<td ><?php echo $Val['libelleh'] ?></td>
						<td ><?php echo $Val['unite'] ?></td>
						<td ><?php echo $Val['prix'] ?></td>
				</tr>
					
				<?php $i++; } ?>
			</table>
		</div>
        </div><!--/.col-xs-12.col-sm-9-->

      </div><!--/row-->

      <hr>

<footer>
  	<div class="col-sm-8 col-sm-offset-2 text-center">
      <hr>
      <ul class="list-inline center-block">
        <li><a href="http://facebook.com/Machouille"><img src="./vue/images/fb.png" style="width:50px; height:50px;"></a></li>
              <li><a href="http://twitter.com/Machouille"><img src="./vue/images/tw.png" style="width:50px; height:50px;"></a></li>
      </ul>
</div>

        </div>
      </footer>

    </div><!--/.container-->
  </body>
</html>