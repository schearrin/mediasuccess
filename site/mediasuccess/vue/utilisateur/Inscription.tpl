<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Inscription</title>

    <!-- Bootstrap core CSS -->
    <link href="./vue/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./vue/offcanvas.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php?controle=utilisateur&action=accueil">Mediatheque</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php?controle=utilisateur&action=ident">Connexion</a></li>
			<li><a href="index.php?controle=utilisateur&action=add">Inscription</a></li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->

    <div class="container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-20 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="jumbotron">			
			<p>Inscrivez-vous gratuitement</p>
			<div ng-app="sample">
    <form class="form-horizontal" name="registerForm" method=POST accept-charset="utf-8">
	<div class="form-group">
		     
				<label class="col-md-3 control-label" for="status">Status</label>
				<div class="col-md-4">
					<div class="btn-group" data-toggle="buttons" >
                <label >
                    <input type="radio" name="societe" value="Entreprise"/> Entreprise
				</label >
<label >				
                    <input type="radio" name="societe" value="Particulier"/> Particulier
                </label>
				</div>
        </div>
		</div>
		
		<div class="form-group">
            <label class="col-md-3 control-label" for="nom">Nom</label>
            <div class="col-md-4">
                <input id="nom" type="text" class="form-control" name="nom" ng-model="text" />
            </div>
        </div>
		
		<div class="form-group">
            <label class="col-md-3 control-label" for="prenom">Prénom</label>
            <div class="col-md-4">
                <input id="prenom" type="text" class="form-control" name="prenom" ng-model="text" />
            </div>
        </div>
		
        <div class="form-group">
            <label class="col-md-3 control-label" for="email">Email</label>
            <div class="col-md-4">
                <input id="email" type="email" class="form-control" name="email" ng-model="email" />
            </div>
        </div>

		<div class="form-group">
            <label class="col-md-3 control-label" for="tel">Téléphone</label>
            <div class="col-md-4">
                <input  type="text"  class="form-control" name="tel" ng-model="tel" />
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label" for="mdp">Mot de passe</label>
            <div class="col-md-4">
                <input id="mdp" type="password" class="form-control" name="mdp" ng-model="Password" />
            </div>
        </div>
	
        <div class="form-group">
            <label class="col-md-3 control-label" for="Adresse">Adresse</label>
            <div class="col-md-4">
                <input id="Adresse" type="text" class="form-control" name="Adresse" ng-model="text" />
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label" for="Ville">Ville</label>
            <div class="col-md-4">
                <input id="Ville" type="text" class="form-control" name="Ville" ng-model="text" />
            </div>
        </div>
        
		<div class="form-group" >
		<label class="col-md-3 control-label" for="Pays">Pays</label>
		<ul class="list-group">
			<div class="col-md-4" >
                <SELECT name="Pays" size="1">
					<OPTION value="France" >France</option>
					<OPTION value="Royaume-Uni">Royaume-Uni</option>
					<OPTION value="Allemagne">Allemagne</option>
					<OPTION value="Italie">Italie</option>
					<OPTION value="Espagne">Espagne</option>
					<OPTION value="Brésil">Brésil</option>
					<OPTION value="Australie">Australie</option>
					<OPTION value="Pologne">Pologne</option>
					<OPTION value="Portugal">Portugal</option>
					<OPTION value="Etats-Unis">Etats-Unis</option>
					<OPTION value="Canada">Canada</option>
					<OPTION value="Amérique du Sud">Amérique du Sud</option>
				</SELECT>
            </div>
		</ul>	
        </div>
        
        <div class="form-group">
            <div class="col-md-offset-3 col-md-9">
                <input type="submit" class="btn btn-default" value="S'inscrire" />
            </div>
        </div>
		    </form>
		</div>
		</div>
</div>
</div>

      <hr>
 <footer>
        <p>Copyright &copy;Tongasoa RAFANOMEZANTSOA Kahina HADJALI Sophie CHEANG ; All Rights Reserved</p>
      </footer>
    </div><!--/.container-->
  </body>
</html>
