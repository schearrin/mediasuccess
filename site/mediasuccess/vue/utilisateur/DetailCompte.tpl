<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Détail Compte</title>

    <!-- Bootstrap core CSS -->
    <link href="./vue/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./vue/offcanvas.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php?controle=utilisateur&action=accueil">Catalogue</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
			<!--<li><a href="index.php?controle=contact&action=listeContact">Contacts</a></li>!-->
			<!--<li><a href="index.php?controle=bloc&action=listeBloc">Mur</a></li>!-->
			<li><a href="index.php?controle=panier&action=detailPanier">Mon Panier</a></li>
			<li><a href="index.php?controle=historique&action=detailHistorique">Historique</a></li>
			<li><a href="index.php?controle=utilisateur&action=detailCompte">Compte</a></li>
            <li><a href="index.php?controle=utilisateur&action=deco">Déconnexion</a></li>
					  		                <SELECT name="Pays" size="1">
					<OPTION value="France" >Français</option>
					<OPTION value="Royaume-Uni">Anglais</option>
					<OPTION value="Allemagne">Allemand</option>
					<OPTION value="Italie">Italien</option>
					<OPTION value="Espagne">Espagnol</option>
					<OPTION value="Brésil">Portugais</option>
				</SELECT>
          </ul>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->

    <div class="container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
            <h2><?php echo $user[0]["nom"]?>  <?php echo $user[0]["prenom"] ?></h2>
			<div class="container-fluid" style="background-color:#DDEDEB; border-radius:10px">

			<table class="table table-bordered table-condensed table-body-center" >
					<tbody>
					
					<tr>
						<td>Societe:</td>
						<td><?php echo $user[0]['societe'] ?></td>
					</tr>
					
					<br>

					<tr>
						<td>Nom:</td>
						<td><?php echo $user[0]['nom'] ?></td>
					</tr>
					
					<tr>
						<td>Prenom:</td>
						<td><?php echo $user[0]['prenom'] ?></td>
					</tr>
					
					<tr>
						<td>Mail:</td>
						<td><?php echo $user[0]['email'] ?></td>
					</tr>
					
					<tr>
						<td>Adresse:</td>
						<td><?php echo $user[0]['Adresse'] ?></td>
					</tr>		
					
					<tr>
						<td>Telephone:</td>
						<td><?php echo $user[0]['Tel'] ?></td>
					</tr>
					
					<tr>
						<td>Ville:</td>
						<td><?php echo $user[0]["Ville"] ?></td>
					</tr>
					
					<tr>
						<td>Pays:</td>
						<td><?php echo $user[0]["Pays"] ?></td>
					</tr>	
					
				</tbody></table>
				<br>
		  </div>
        </div><!--/.col-xs-12.col-sm-9-->

      </div><!--/row-->

      <hr>
<footer>
  	<div class="col-sm-8 col-sm-offset-2 text-center">
      <hr>
      <ul class="list-inline center-block">
        <li><a href="http://facebook.com/Machouille"><img src="./vue/images/fb.png" style="width:50px; height:50px;"></a></li>
              <li><a href="http://twitter.com/Machouille"><img src="./vue/images/tw.png" style="width:50px; height:50px;"></a></li>
      </ul>
</div>
      </footer>

    </div><!--/.container-->
  </body>
</html>
