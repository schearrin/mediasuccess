<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Connexion</title>

    <!-- Bootstrap core CSS -->
    <link href="./vue/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./vue/offcanvas.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php?controle=utilisateur&action=accueil">Mediatheque</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php?controle=utilisateur&action=ident">Connexion</a></li>
			<li><a href="index.php?controle=utilisateur&action=add">Inscription</a></li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->

    <div class="container">

      <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="jumbotron">
            <h3>Entrez-vos informations :</h3>
			</br>
			<div class="container">
                    
            <form name="login" role="form" class="form-horizontal" method=POST accept-charset="utf-8">
                <div class="form-group">
                <div class="col-md-8"><input name="nom" placeholder="Nom" class="form-control" type="text" id="UserUsername"/></div>
                </div> 
                
                <div class="form-group">
                <div class="col-md-8"><input name="num" placeholder="Mot de passe" class="form-control" type="password" id="UserPassword"/></div>
                </div> 
                
                <div class="form-group">
                <div class="col-md-offset-0 col-md-8"><input  class="btn btn-success btn btn-success" id="btp_envoie" type="submit" value="Envoyer"/></div>
                </div>    
            </form>
        </div>
          </div>
        </div><!--/.col-xs-12.col-sm-9-->

      </div><!--/row-->

      <hr>

      <footer>
        <p>Copyright &copy;Tongasoa RAFANOMEZANTSOA Kahina HADJALI Sophie CHEANG ; All Rights Reserved</p>
      </footer>

    </div><!--/.container-->
  </body>
</html>
