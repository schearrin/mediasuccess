<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Catalogue</title>

    <!-- Bootstrap core CSS -->
    <link href="./vue/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./vue/offcanvas.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php?controle=utilisateur&action=accueil">Catalogue</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
         <ul class="nav navbar-nav">
			<li><a href="index.php?controle=panier&action=detailPanier">Mon Panier</a></li>
			<li><a href="index.php?controle=historique&action=detailHistorique">Historique</a></li>
			<li><a href="index.php?controle=utilisateur&action=detailCompte">Compte</a></li>
            <li><a href="index.php?controle=utilisateur&action=deco">Déconnexion</a></li>
		</ul>	
		                <SELECT name="Pays" size="1">
					<OPTION value="France" >Français</option>
					<OPTION value="Royaume-Uni">Anglais</option>
					<OPTION value="Allemagne">Allemand</option>
					<OPTION value="Italie">Italien</option>
					<OPTION value="Espagne">Espagnol</option>
					<OPTION value="Brésil">Portugais</option>
				</SELECT>
        </div>
          </ul>
		  
        </div><!-- /.nav-collapse -->
		
      </div><!-- /.container -->
    </nav><!-- /.navbar -->

       <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2>Films</h2>
          <p>Choisissez nos films à mâcher saveur fruités parmi notre catalogue : Cerise, Pomme, Fraise , ...</p>
          <p><a href="./index.php?controle=catalogue&action=affcatalogueF"><img src="./vue/Fruiter.jpg"></a></p>
        </div>
        <div class="col-md-4">
          <h2>Livres</h2>
          <p>Choisissez nos livres à mâcher saveur crémeux parmi notre catalogue : Praliné, Tiramisu, Lait-Fraise,...</p>
          <p><a href="./index.php?controle=catalogue&action=affcatalogueC"><img src="./vue/crémeux.jpg" style="width:250px; height:250px;"></a></p>
       </div>
        <div class="col-md-4">
          <h2>Musiques</h2>
          <p>Choisissez nos musiques à mâcher tri-goûts parmi notre catalogue : Lait-Fraise-Cerise, Lait-Pomme-Cerise,...</p>
          <p><a href="./index.php?controle=catalogue&action=affcatalogueT"><img src="./vue/Fruiter.jpg"></a></p>
        </div>
      </div>
	<hr>
	  	<div class="col-xs-16 col-sm-16">
							  <div class="row">
				<?php $i=1;
				foreach($catalogue as $Val){ ?>
    <form class="form-horizontal" name="registerForm" method=POST accept-charset="utf-8">
				        <div class="col-md-4">
						<!--<?php echo $i ?>-->
						<p style="text-align:center; background-color:red;"><?php echo $Val['libelle'] ?></p>
						<p style="text-align:center"><img src="./vue/Fruiter.jpg" style="width:150px; height:150px;"></p>
						<p style="text-align:center"><input id="nombre" type="number" name="nombre"  min="0"/></p>
						<p style="text-align:center; background-color:red"><?php echo $Val['Prix'] ?> euros</p>
							<div class="col-md-offset-0 col-md-4"><p>
							<p style="margin-left:100px;"><input type="submit" class="btn btn-default" value="Ajouter au panier" /></p>
					</div>
				        </div>		
					</form>			
				<?php $i++; } ?>
					</div>	
</div>		
			<div class="responsive-table-line" style="margin:100px auto;width:75em;">
				<table class="table table-bordered table-condensed table-body-center" >
				<tr>
					<td class="info"></td>
					<td class="info">Famille</td>
					<td class="info">libelle</td>
					<td class="info">code</td>
					<td class="info">unite</td>
					<td class="info">Prix</td>
					<td class="info">Action</td>
				</tr>
				
				<?php $i=1;
				foreach($catalogue as $Val){ ?>
				<tr>
						<td class="info"><?php echo $i ?></td>
						<td><?php echo $Val['Famille'] ?></td>
						<td><?php echo $Val['libelle'] ?></td>
						<td><?php echo $Val['code_prd'] ?></td>
						<td><input id="nombre" type="number" name="nombre"  min="0"/></td>
						<td><?php echo $Val['Prix'] ?></td>
						<td class="action">
							<div class="col-md-offset-0 col-md-5"><p>
							<a class="btn btn-default" type="submit" role="button" href="index.php?controle=catalogue&action=addPanier&code=<?php echo $Val['code_prd'] ?>&libelle=<?php echo  $Val['libelle'] ?>&prix=<?php echo $Val['Prix']?>&unite=5">Ajouter au panier</a></p>
</div>
						</td>
				</tr>			
				<?php $i++; } ?>
			</table>
    </div>
	      </div>

    </div> <!-- /container -->
				<hr>  
<footer>

  	<div class="col-sm-8 col-sm-offset-2 text-center">
      <hr>
      <ul class="list-inline center-block">
        <li><a href="http://facebook.com/Machouille"><img src="./vue/images/fb.png" style="width:50px; height:50px;"></a></li>
              <li><a href="http://twitter.com/Machouille"><img src="./vue/images/tw.png" style="width:50px; height:50px;"></a></li>
      </ul>
</div>

      </footer>

  </body>
</html>
