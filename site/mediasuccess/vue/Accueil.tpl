
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Machouille - Page Accueil</title>

    <!-- Bootstrap Core CSS -->
    <link href="./vue/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./vue/dist/css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

</head>

<body>


    <div class="intro-header">
	
    <div class="navbar-wrapper">
      <div class="container">
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php?controle=utilisateur&action=accueil">Mediatheque</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
				<li><a href="index.php?controle=utilisateur&action=ident">Connexion</a></li>
				<li><a href="index.php?controle=utilisateur&action=add">Inscription</a></li>
                <li><a href="#contact">Livres</a></li>
				<li><a href="#contact">Films</a></li>
				<li><a href="#contact">Musiques</a></li>
				
              </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>
	
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h1>Mediatheque</h1>
						<h3>Une grande bibliotheque de livres, films et musiques</h3>
                        <hr class="intro-divider">
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->

    <div class="content-section-a">

        <div class="container">

            <div class="row">
				 <div class="column1">
                      <h2>Horaires d'ouverture</h2>
                                <div class="col-content">
								<div class="image_wrapper"><img src="./vue/images/col1.jpg"></div>La médiathèque ouvre ses portes du lundi <br>au vendredi de 10h à 20h. </div>
                  </div>
				    <div class="column2">
                      <h2>Adresse</h2>
                                <div class="col-content"> <div class="image_wrapper"><img src="./vue/images/col2.jpg"></div> 15 Rue Georges Clemenceau, 91400 Orsay <br> Bâtiment 640 PUIO <br> RER : B <br> Bus : 91-06C, 91-06B, 9, 7  </div>
                 </div>
                    <div class="column3">
                      <h2>Contact</h2>
                                <div class="col-content"> <div class="image_wrapper"><img src="./vue/images/col3.jpg"></div>Téléphone : 01 69 15 67 50 <br> Email : contact@mediatheque.esy.es </div>
                </div>
            </div>
  </div>

        </div>

    </div>

    <!-- Footer -->
    <footer>
	<hr>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li>
                            <a href="index.php?controle=utilisateur&action=accueil">Mediatheque</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="index.php?controle=utilisateur&action=ident">Connexion</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="index.php?controle=utilisateur&action=add">Inscription</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
						<li><a href="#contact">Livres</a></li>
						<li><a href="#contact">Films</a></li>
						<li><a href="#contact">Musiques</a></li>
                    </ul>
                    <p class="copyright text-muted small">Copyright &copy;Tongasoa RAFANOMEZANTSOA Kahina HADJALI Sophie CHEANG ; All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>

