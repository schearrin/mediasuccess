<?php 

/*controleur utilisateur.php :
  fonctions-action de gestion des utilisateurs
*/

function ident () {
	
	if(count($_POST) == 0)
	{
		require "vue/utilisateur/Connexion.tpl";
	}
	else
	{
		$nom = htmlentities($_POST['nom']);
		$num = htmlentities($_POST['num']);
		
		if(verif_syntaxe_ident($nom,$num))
		{
			$_SESSION['profilU'] = 'ok';
			$_SESSION['nom'] = $nom;
			$_SESSION['num'] = $num;
			$tab=array();
			header('Location:index.php?controle=utilisateur&action=accueil'); 
		}
		else
		{
			echo " Vous n'avez pas les droits pour avoir accès à cette page !";
			require "vue/utilisateur/Connexion.tpl";
		}
	}
}

function detailCompte() {
	include "modele/utilisateurBD.php";
	
	$user = get_info_compte($_SESSION['nom'],$_SESSION['num']);
	
	require "vue/utilisateur/DetailCompte.tpl";
}

function accueil () {
	if(isset($_SESSION['profilU'])){
		$catalogue=array();
		require "vue/Accueil2.tpl";
	}
	else
		require "vue/Accueil.tpl";
}

function deco() {
	session_destroy();
	require "vue/Accueil.tpl";
}

function add () {
	if(count($_POST) == 0)
	{
		require "vue/utilisateur/Inscription.tpl";
	}
	else
	{
		$soc = htmlentities($_POST['societe']);
		$nom = htmlentities($_POST['nom']);
		$pre = htmlentities($_POST['prenom']);
		$email = htmlentities($_POST['email']);
		$tel = htmlentities($_POST['tel']);
		$mdp = htmlentities($_POST['mdp']);
		$add = htmlentities($_POST['Adresse']);
		$ville = htmlentities($_POST['Ville']);
		$pays = htmlentities($_POST['Pays']);
		$tab=array();
		if(ajouter_db($soc,$nom,$pre,$email,$tel,$mdp,$add,$ville,$pays))
		{
			$_SESSION['profilU'] = 'ok';
			$_SESSION['nom'] = $nom;
			$_SESSION['num'] = $mdp;
			header('Location:index.php?controle=catalogue&action=affcatalogueF');
		}
		else
		{
			echo "Erreur impossible de vous ajouter !";
			require "vue/utilisateur/Inscription.tpl";
		}
	}
}

// vérification syntaxique des saisies
function verif_syntaxe_ident($nom,$num) {
	require 'modele/utilisateurBD.php';
	return verif_bd($nom,$num);
} 

function ajouter_db($soc,$nom,$pre,$email,$tel,$num,$add,$ville,$pays) {
	require 'modele/utilisateurBD.php';
	if($num == "" && $nom == "")
		echo "Erreur inscription";
	else
		return add_bd($soc,$nom,$pre,$email,$tel,$num,$add,$ville,$pays);
} 
