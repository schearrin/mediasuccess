<?php include 'header.php';?>
<div class="container">
  <div class="content inside-page about">
    <div class="breadcrumb"><a href="index.php">Home</a> / Mon compte</div>

   <!-- titre -->
   <h2 class="title">Mon compte</h2>

   <div class="row">
    <!-- ce qui se trouve sur la gauche -->
    <div class="col-sm-4">
      <h3>Profil</h3>
      <ul class="compte-menu-vertical">
        <li class="compte-mv-item"><a href="cpteadh_informations.php">Informations</a></li>
      </ul>
      <br /><br />

      <h3>Menu</h3>
      <ul class="compte-menu-vertical">
        <li class="compte-mv-item"><a href="cpteadh_reservation.php">Liste des réservations</a></li>
        <li class="compte-mv-item"><a href="cpteadh_prolongation.php">Liste des emprunts</a></li>
        <li class="compte-mv-item"><a href="cpteadh_achat.php">Historique d'achat</a></li>
      </ul>
      <br /><br />
    </div>
    <!-- ce qui se trouve sur la droite -->  	
    <div class="col-sm-8">


      <h3>Informations</h3>
      <p>
        <h4 align="center">Bonjour <?php echo $_SESSION['adherentprenom']; ?> <?php echo $_SESSION['adherentnom']; ?> !</h4>
      </p>

      <div class="location col-sm-10 col-sm-offset-1 col-sm-12">

        <table class="compte-menu-vertical" style="font-size:125%;">
         <tr>
          <td><b>Login</b></td>
          <td><?php echo $_SESSION['logadherent']; ?></td>
        </tr>
        <tr>
          <td><b>Nom</b></td>
          <td><?php echo $_SESSION['adherentnom']; ?></td>
        </tr>
        <tr>
          <td><b>Prénom</b></td>
          <td><?php echo $_SESSION['adherentprenom']; ?></td>
        </tr>
        <tr>
          <td><b>Email</b></td>
          <td><?php echo $_SESSION['emailadherent']; ?></td>
        </tr>
        <tr>
          <td><b>Téléphone</b></td>
          <td>0<?php echo $_SESSION['teladherent']; ?></td>
        </tr>
      </table>

    </div>


    </div>
  </div>


</div>

</div>
<?php include 'footer.php';?>