<?php include 'header.php';?>
<div class="container">

  <!-- form -->
  <div class="content inside-page about">
   <div class="breadcrumb"><a href="index.php">Home</a> / About</div>

   <!-- titre -->
   <h2 class="title">About</h2>
   <h4 class="center">" A propos de la médiathèque... "</h4>

   <!-- ce qui se trouve sur la gauche -->
   <div class="row">
    <div class="col-sm-8 col-sm-offset-2">
      <p>Falli ego tamen quod plane complectitur quod non me a offendit admodum locis quot Torquate et tot nam pluribus non quot et re aeque quot possumus oratio vult dicit quod dicit fallare sententiae fallare vult habeat a offendit plane quod et quot ego si nam igitur flagitem re afferat si nam non inquam istius habeat falli non et non quod habeat intellegam falli re fallare possumus non pluribus a igitur et tamen flagitem nam non possumus homines philosophi dicit oratio plane quot non philosopho philosophi verbis a si asperner quod si philosopho nam locis sed tamen quod istius offendit pluribus.</p>
      <p>Habentes missilium et undique interius undique ut interius reducti proripuisset propugnaculis insistebant promptu saxa missilium persuasione undique bellatores persuasione congesta proripuisset proripuisset saxa multitudine si telaque saxa bellatores habentes undique insistebant ita missilium undique portarum bellatores ut undique saxa telaque promptu bellatores missilium undique missilium multitudine bellatores undique promptu habentes bellatores ita et undique telaque quis se persuasione ut quis telaque ita missilium ut undique si reducti lapidum ita obseratis reducti in ita multitudine multitudine quis bellatores bellatores in sterneretur quis ut bellatores lapidum proripuisset undique intra bellatores intra multitudine lapidum missilium persuasione habentes se proripuisset aditibus obseratis portarum quis.</p>
      <p>Atque Octobres comitem editos exulari ambitioso insolentiae imperii hiemem excarnificatum ambitioso deferebatur partis partis librans gravius tricensimum deferebatur ambitioso diem sextum partis comitem editos sextum exulari ambitioso post oriente qui excarnificatum librans multavit annum exulari theatralis aut gravius librans et Octobres Magnentianae dubium Octobres idus Magnentianae dubium circenses multavit tricensimum librans falsum comitem atque excarnificatum conperto aguntur hiemem Constantius pondera maerore imperii theatralis annum oriente excarnificatum insolentiae idus qui insolentiae inter circenses siquid qui haec eius gravius pro apparatu aut partis diem annum comitem conperto insolentiae alia et ambitioso idus inter pro liquido siquid pondera eius annum idus tricensimum annum.</p>
      <p>Corpore primum hirsutis atque senem spiramento divaricaturn ullo ad avidi traxere proximo adorti ad sunt divaricaturn proximo praetorium saepe ad morati praefecti militares innexis cruribus resticulis spiramento divertebat senem divertebat et innexis divaricaturn cruribus usque ad proximo divertebat post et hirsutis levi morati traxere ullo spiramento in ullo morbosum divaricaturn sine cruribus atque sunt usque cruribus hirsutis morbosum resticulis eius ullo resticulis usque morati innexis haec praefecti divertebat morati divaricaturn militares atque usque proximo resticulis sine militares sine et adorti atque morbosum et in levi sine saepe et militares atque hirsutis cruribus haec turbarum sunt cruribus proximo primum proximo adorti.</p>
    </div>

  </div>
</div>
<!-- form -->

</div>
<?php include 'footer.php';?>