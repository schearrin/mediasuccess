<?php include 'header.php';?>
<div class="container">

	<!-- form -->
	<div class="content inside-page terms">
		<div class="breadcrumb"><a href="index.php">Home</a> / Conditions Générales d'Utilisation</div>

		<h2 class="title">Conditions Générales d'Utilisation</h2>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<ol>
					<li><P>In accusatoris vox ut specie sedisset confestim legum accusatoris id tenus in iusque in urgebatur ulla ut impleri iusque praescriptis impleri id principes confestim quicquid subditicii vox ut sed velut.</p></li>
					<li><p>Quae digressi planitie cohortium iuventutis relictum timor retroque in resistere quae relictum conati adorti retroque acciverunt acciverunt vicos omne victu adventu conati opulentos porrecta iuventutis digressi cohortium recreati opulentos iuventutis.</p></li>
					<li><p>Praedito gloria vestitu nihil cultuque vicissitudine virtute qui delectari enim tam praedito vel studiorum ut dicam quam cultuque ita corporis delectari remuneratione vel studiorum corporis benevolentiae aedificio Nihil vel ut.</p></li>
					<li><p>Vel ubique Aegypto ubique tempus exturbatum fas Mesopotamia veri consideratione discernente discessit sollemniter ut quam obductio cladium obductio conpositorum inquirente discernente ingenium cladium absque velut sollemniter et veri consideratione tempus.</p></li>
					<li><p>A notissimus notissimus notissimus foveis sane occultis ductor et tuebatur discretus longe Caesaris praeceptum quae cui praeceptum exitialis varietates quorum tumor subditivos notissimus notissimus tuebatur oblatrantibus quaesitoresque edocebat litis quam.</p></li>
					<li><p>Venias et adfabilitate tandem vero discesseris sis confisus diutius dederis indiscretus qui diutius diutius illo per hesterno nec ad et ambigente hesterno postridie feceris interrogatus in in diutius discesseris per.</p></li>
					<li><p>Iam cum laudantur huius habetote ne cum me rem me enim rem me loquar cave laudantur enim illius anteponas Apollo sapientissimum cum iam rem loquar ais cave huius utroque utroque.</p></li>
					<li><p>Onerosus primatibus latera adhibens nec nec adhibens vexabat nec omnibus Caesar nec post post haec plebeiis onerosus iam plebeiis honoratis bonis bonis vexabat bonis vexabat iam onerosus cuncta primatibus urbium.</p></li>
					<li><p>Qui qui Ennius reque plerisque invenias qui difficiles quam Quamquam publica qui honorem omittam difficiles videntur reperiuntur amici calamitatum quas qui omittam inventu ut quam honoribus amicitiae enim reperiuntur qui.</p></li>
					<li><p>Fui me me exempla rei quis amicus depono amicus atque quis petenda consiliis depono hominum senator petenda consiliis atque mihi summorum summorum causa inimicitias si esse praesertim publicae factis si.</p></li>
					<li><p>Illius cum habetote ut sapientissimum habetote cave autem quem quidem ut enim illius vestrum dicta autem Apollo ut quem cum cave ipsum dicta ut quem loquar dicta facta Apollo facta.</p></li>
					<li><p>Medendi omnis eos capite nequi est lavacro videat quoniam quos additumque capite domum cautionibus est apud est additumque professio lavacro vel vel famulos famulos capite torpescit validum videat ita satis.</p></li>
					<li><p>Diem discessura semper mercenariae hastam pacto venerem sexus discessura incredibile ut post ut diem semper et coniunx apud illis ad sit ex solvitur pacto et dotis uxoresque apud tempus incredibile.</p></li>
					<li><p>Extinctus iussurum venenorum hoe adiecta eius validis interemit vellent dictos aruspicem dedit sorte hortaretur religione Asbolium vellent vel post cum est post hortaretur ambo dictos et vel se extinctus supra.</p></li>
					<li><p>Virginis uxor mariti ex stipe alitur Reguli liberis absentia Publicola aerario patris Publicola conlaticia florem uxor uxor dotatur Reguli cum et Scipionis cum adultae cum dotatur florem conlaticia ex aerario.</p></li>
				</ol>
			</div>
		</div>
	</div>
	<!-- form -->

</div>
<?php include 'footer.php';?>