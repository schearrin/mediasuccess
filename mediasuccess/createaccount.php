<?php include 'header.php';?> 


<div class="container">
	<!-- form -->
	<div class="content inside-page create-account">
		<h2 class="title">Créer un compte adhérent</h2>
		<div class="row">

			<?php

//1 verification formulaire
			if(isset($_POST['nom'], $_POST['prenom'], $_POST['motdepasse'], $_POST['motdepasseverification'], $_POST['email'], $_POST['telephone']) and $_POST['nom']!='' and $_POST['prenom']!='')
			{
				$_POST['nom'] = mysqli_real_escape_string($connexionbdd, $_POST['nom']);
				$_POST['prenom'] = mysqli_real_escape_string($connexionbdd, $_POST['prenom']);
				$_POST['motdepasse'] = mysqli_real_escape_string($connexionbdd, $_POST['motdepasseverification']);
				$_POST['telephone'] = mysqli_real_escape_string($connexionbdd, $_POST['telephone']);
				$_POST['email'] = mysqli_real_escape_string($connexionbdd, $_POST['email']);
            
              if(preg_match('#^[a-zA-Z]+((-| )?[a-zA-Z]+)?$#', $_POST['nom']) && preg_match('#^[a-zA-Z]+((-| )?[a-zA-Z]+)?$#', $_POST['prenom'])) {
	       //2 enleve lechappement si get_magic_quotes_gpc est active
				
				if(get_magic_quotes_gpc())
				{
					$_POST['nom'] = stripslashes($_POST['nom']);
					$_POST['prenom'] = stripslashes($_POST['prenom']);
					$_POST['motdepasse'] = stripslashes($_POST['motdepasse']);
					$_POST['motdepasseverification'] = stripslashes($_POST['motdepasseverification']);
					$_POST['email'] = stripslashes($_POST['email']);
				}


	//3 verification mot de passe identique
				if($_POST['motdepasse']==$_POST['motdepasseverification'])
				{
		//4 verification nombre de caracteres dans mot de passe
					if(strlen($_POST['motdepasse'])>=6)
					{

			//7 verification email
						if(preg_match('#^(([a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+\.?)*[a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+)@(([a-z0-9-_]+\.?)*[a-z0-9-_]+)\.[a-z]{2,}$#i',$_POST['email']))
						{
			//8 verification email unique
							if(verif_mailunique($_POST['email'])){

			//5 verification telephone
							if(preg_match(('`^0[1-9][0-9]{8}$`'), $_POST['telephone'])){

								$nom = $_POST['nom'];
								$prenom = $_POST['prenom'];
								$motdepasse = sha1($_POST['motdepasse']);
								$email = $_POST['email'];
								$telephone = $_POST['telephone'];

								$temp = time();
								$temp2 = 'adherent';
								$loginadherent = generelogin($temp2,$prenom, $nom, $temp);
                                 
					//6 enregistre dans la base de donnee
								if(mysqli_query($connexionbdd, 'insert into adherent (loginA, nom, prenom, telephone, email, mdp) values ("'.$loginadherent.'","'.$nom.'", "'.$prenom.'", "'.$telephone.'", "'.$email.'", "'.$motdepasse.'")'))
								{
									$form = false;
									?>
									<div class="message">
										Vous avez bien &eacute;t&eacute; inscrit. Vous pouvez dor&eacute;navant vous connecter.<br /><br />
										Votre identifiant est le suivant :
										<?php
										echo "<h3>".$loginadherent."</h3>";

										mysqli_close($connexionbdd);
									}
					else //else 6
					{
						$form = true;
						$message = 'Une erreur est survenue lors de l\'inscription.';
					}
					//fin 6

				} else{ // else 5

					$form = true;
					$message = 'Le numero de telephone n\'est pas correct ou non saisi.';				

				}
				//fin 5
			}else{
					$form = true;
					$message = 'L\'email que vous avez entr&eacute; n\'est pas unique.';
			}

			}else{ //else 7
				$form = true;
				$message = 'L\'email est non valide ou non saisi.';
			}
			//fin else 7

		} // else 4
		else
		{
			$form = true;
			$message = 'Le mot de passe est non saisi ou contient moins de 6 caract&egrave;res .';
		}
		//fin 4

	} //else 3
	else
	{
		//Sinon, on dit que les mots de passes ne sont pas identiques
		$form = true;
		$message = 'Les mots de passe que vous avez entr&eacute; ne sont pas identiques.';
	}
    //fin 3
   }
   else{

   	    $form = true;
   	    $message = 'Le nom ou prénom contiennent des caract&egraveres non autoris&eacutes';
   }
} //else 1
else 
{
	$form = true;
}
//fin 1



// affiche les messages d'erreurs
if($form)
{
	if(isset($message))
	{
		echo '<div class="message">'.$message.'</div>';
	}
	?>

	<!-- affiche le formulaire -->
	<form class="form-horizontal col-sm-6 col-sm-offset-3" method="post" action="createaccount.php">
		<div class="panel-body">

			<div class="form-group">
				<label for="inputEmail3" class="col-sm-4 control-label">Nom</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="inputEmail3" name="nom" value="<?php if(isset($_POST['nom'])){echo htmlentities($_POST['nom'], ENT_QUOTES, 'UTF-8');} ?>" />
				</div>				
			</div>

			<div class="form-group">
				<label for="inputEmail3" class="col-sm-4 control-label">Prénom</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="inputEmail3" name="prenom" value="<?php if(isset($_POST['prenom'])){echo htmlentities($_POST['prenom'], ENT_QUOTES, 'UTF-8');} ?>" />
				</div>				
			</div>


			<div class="form-group">
				<label for="inputEmail3" class="col-sm-4 control-label">Adresse email</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="inputEmail3" name="email" value="<?php if(isset($_POST['email'])){echo htmlentities($_POST['email'], ENT_QUOTES, 'UTF-8');} ?>" />
				</div>				
			</div>	

			<div class="form-group">
				<label for="inputEmail3" class="col-sm-4 control-label">Téléphone</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="inputEmail3" name="telephone" value="<?php if(isset($_POST['telephone'])){echo htmlentities($_POST['telephone'], ENT_QUOTES, 'UTF-8');} ?>" />
				</div>				
			</div>	

			<div class="form-group">
				<label for="inputEmail3" class="col-sm-4 control-label">Mot de passe<span class="small">(6 caract&egrave;res min.)</span></label>
				<div class="col-sm-8">
					<input type="password" class="form-control" id="inputEmail3" name="motdepasse">
				</div>				
			</div>

			<div class="form-group">
				<label for="inputEmail3" class="col-sm-4 control-label">Re-entrer le mot de passe<span class="small">(v&eacute;rification)</span></label>
				<div class="col-sm-8">
					<input type="password" class="form-control" id="inputEmail3" name="motdepasseverification">
				</div>				
			</div>	


			<button class="btn btn-danger pull-right">Valider</button>


		</div>
	</form>
	<?php
}
?>
</div>
</div>
</div>
</div>


<?php include 'footer.php';?> 