<?php
session_start();
include 'connect_base.php';
include 'fonctions_bdd.php';
include 'fonctions_nonbdd.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Mediasuccess</title>

<!-- Menu compte -->
<link rel="stylesheet" type="text/css" href="assets/stylenavmenu.css" />

<!-- Google fonts -->
<link href='http://fonts.googleapis.com/css?family=Old+Standard+TT:400,400italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>

<!-- font awesome -->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<!-- bootstrap -->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" />

<!-- uniform -->
<link type="text/css" rel="stylesheet" href="assets/uniform/css/uniform.default.min.css" />

<!-- animate.css -->
<link rel="stylesheet" href="assets/wow/animate.css" />

<!-- favico -->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="assets/style.css">

</head>

<body>
<!-- header -->
<nav class="navbar  navbar-inverse navbar-fixed-top" role="navigation">
  <div class="clearfix">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php"><img src="images/logo.png"  alt="logo"><h1 class="hide">Médiathèque</h1></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
      
      <ul class="nav navbar-nav">
        <li><a href="index.php">Accueil</a></li>       
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Livres<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="collections.php?typeoeuvre=livre&typetypeoeuvre=BD">BD</a></li>
            <li><a href="collections.php?typeoeuvre=livre&typetypeoeuvre=Journaux">Journaux</a></li>
            <li><a href="collections.php?typeoeuvre=livre&typetypeoeuvre=Magazines">Magazines</a></li>
            <li><a href="collections.php?typeoeuvre=livre&typetypeoeuvre=Romans">Romans</a></li>
             
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Musiques<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="collections.php?typeoeuvre=musique&typetypeoeuvre=Electro">Electro</a></li>
            <li><a href="collections.php?typeoeuvre=musique&typetypeoeuvre=Pop">Pop</a></li>
            <li><a href="collections.php?typeoeuvre=musique&typetypeoeuvre=Rap">Rap</a></li>
            <li><a href="collections.php?typeoeuvre=musique&typetypeoeuvre=Rnb">Rnb</a></li>
            <li><a href="collections.php?typeoeuvre=musique&typetypeoeuvre=Rock">Rock</a></li>        
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Films<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="collections.php?typeoeuvre=film&typetypeoeuvre=Action">Action</a></li>
            <li><a href="collections.php?typeoeuvre=film&typetypeoeuvre=Comedie">Comédie</a></li>
            <li><a href="collections.php?typeoeuvre=film&typetypeoeuvre=Horreur">Horreur</a></li>
            <li><a href="collections.php?typeoeuvre=film&typetypeoeuvre=Policier">Policier</a></li>
            <li><a href="collections.php?typeoeuvre=film&typetypeoeuvre=Romance">Romance</a></li>        
          </ul>
        </li>

<?php
   if(isset($_SESSION['logadherent']))
   {
?>
        <!-- acces adherent -->
        <li><a href="cpteadh_informations.php">Mon compte</a></li>
        <li><a href="deconnexion.php">Se déconnecter</a></li>
<?php
}
elseif(isset($_SESSION['loggestionnaire']))
    {
?>
        <!-- acces gestionnaire -->
        <li><a href="cptegest_informations.php">Administration</a></li>
        <li><a href="deconnexion.php">Se déconnecter</a></li>
<?php
}
else
    {
?>
        <!-- acces visiteur -->
        <li><a href="connexion.php">Se connecter</a></li>
        <li><a href="createaccount.php">S'inscrire</a></li>
<?php
    }
?>
      </ul>


      <!-- <ul class="nav navbar-nav">
       <li><a href="#"  data-toggle="modal" data-target="#login"><span class="glyphicon glyphicon-user"></span> S'inscrire / Connexion</a></li> -->
      
      <!-- Barre de recherche -->
      <form class="navbar-form navbar-left searchbar" role="search" action ="search.php" method ="post">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Rechercher des médias" name="Recherche">
        </div>
        <button type="submit" class="btn btn-inverse">Ok</button>
      </form>

        

        <!-- Panier d'achat
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span> Cart <span class="cart-counter">8</span> <span class="caret"></span></a>
          <div class="dropdown-menu mini-cart">
          <div class="row product-list">
            <div class="col-xs-3"><a href="#"><img src="images/products/1.jpg" class="img-responsive" alt="product"></a></div>
            <div class="col-xs-7"><a href="#">White V-neck Tshirt</a></div>
            <div class="col-xs-1"><a href="#"><i class="fa fa-close"></i></a></div>
          </div>

          <div class="row product-list">
            <div class="col-xs-3"><a href="#"><img src="images/products/2.jpg" class="img-responsive" alt="product"></a></div>
            <div class="col-xs-7"><a href="#">White V-neck Tshirt</a></div>
            <div class="col-xs-1"><a href="#"><i class="fa fa-close"></i></a></div>
          </div>

          <div class="clearfix">
          <a href="checkout.php" class="btn btn-primary pull-left">continuer la réservation</a>
          <a href="checkout.php" class="btn btn-danger pull-right">finir</a>
          </div>

          </div>
        </li>
      </ul>
      -->

    </div><!-- Wnavbar-collapse -->
  </div><!-- container-fluid -->
</nav>
<!-- header -->

